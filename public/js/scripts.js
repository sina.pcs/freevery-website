// $(function () {
//     $.material.init();
//     $.material.ripples();
//
//     $.material.input();
//     $.material.radio();
//     $(".shor").noUiSlider({
//         start: 40,
//         connect: "lower",
//         range: {
//             min: 0,
//             max: 100
//         }
//     });
//
//     $(".svert").noUiSlider({
//         orientation: "vertical",
//         start: 40,
//         connect: "lower",
//         range: {
//             min: 0,
//             max: 100
//         }
//     });
// });

var superi = angular.module('superiApplication', ['ng','ui.router','ui.bootstrap','ngAnimate','ngSanitize','ui.select','ngMaterial']);

superi.config(['$stateProvider', '$urlRouterProvider',function($stateProvider,$urlRouterProvider) {
    // An array of state definitions
    // $urlRouterProvider.when('', '/home');
    $stateProvider
        .state("home",{
            name: 'home',
            url: '',
            resolve:{
                categories:function (CategoryService, $transition$,$rootScope) {
                    return CategoryService.getCategory(1).then(function(c) {
                        return c;
                    });
                }
            },
            views: {
                'mainView': {
                    component: 'category',
                }
            }
        })
        .state("home2",{
            name: 'home2',
            url: '/',
            resolve:{
                categories:function (CategoryService, $transition$,$rootScope) {
                    return CategoryService.getCategory(1).then(function(c) {
                        return c;
                    });
                }
            },
            views: {
                'mainView': {
                    component: 'category',
                }
            }
        })
        .state("confirm",{
            name: 'confirm',
            url: '/confirm',
            views: {
                'mainView': {
                    templateUrl: 'app/confirmOrder.html'
                }
            },
        })
        .state("checkout",{
            name: 'checkout',
            url: '/checkout',
            views: {
                'mainView': {
                    templateUrl: 'app/checkout.html'
                }
            },
        })
        .state("login",{
            name: 'login',
            url: '/login',
            views: {
                'popup': {
                    templateUrl: 'app/login.html'
                }
            },
        })
        .state("signup",{
            name: 'signup',
            url: '/signup',
            views: {
                'popup': {
                    templateUrl: 'app/signup.html'
                }
            },
        })
        .state("test",{
            name: 'test',
            url: '/test',
            resolve:{
                product:function (ProductService, $transition$) {
                    // return ProductService.getProduct($transition$.params().productId);
                }
            },
            views: {
                'productDetail': {
                    component: 'product',
                }
            },
        })
        .state("product",{
            name: 'product',
            url: '/product/:productId',
            resolve:{
                product:function (ProductService, $transition$,$rootScope) {
                    // debugger;
                    return ProductService.getProduct($transition$.params().productId).then(function(p) {
                        $rootScope.product = p;
                        return p;
                    });
                }
            },
            views: {
                'popup': {
                    component: 'product',
                }
            },
            data: {
                pageTitle: 'مشخصات محصول'
            }
        })
        .state("category",{
            name: 'category',
            url: '/category/:categoryId',
            resolve:{
                categories:function (CategoryService, $transition$,$rootScope) {
                    return CategoryService.getCategory($transition$.params().categoryId).then(function(c) {
                        return c;
                    });
                }
            },
            views: {
                'mainView': {
                    component: 'category',
                }
            },
            data: {
                pageTitle: 'مشخصات محصول'
            }
        });
}]);

superi.run(function($http,$state,$stateParams,$rootScope,$transitions) {
    $http.get('data/product.json', { cache: true });
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

});

superi.controller('superiController', superiController);

function superiController ($scope,$timeout,$state,$stateParams, $rootScope,$uibModal, $log,ProductService,$transitions,$window) {
    var vm = this;
    $transitions.onBefore({ to: 'product', from: '*' }, function() {
        vm.showPopup = true;
        vm.productDetail = $rootScope.product;
    });
    $transitions.onBefore({ to: 'login', from: '*' }, function() {
        vm.showPopup = true;
        $rootScope.product = false;
    });
    // $transitions.onFinish({ to: '*', from: '*' }, function() {
    //     $.material.init();
    // });
    $transitions.onBefore({ to: 'signup', from: '*' }, function() {
        vm.showPopup = true;
        $rootScope.product = false;
    });
    this.closePopup = function (event) {
        var c = document.getElementById('popupBack');
        var c2 = document.getElementById('closeButton');
        var c3 = document.getElementById('closeButton2');
        if(event.target == c || event.target == c2 || event.target == c3){
            $rootScope.product = false;
            vm.showPopup = false;
            // $state.go('home');
            $window.history.back();
        }
    }
    this.showProductDetail = function (product) {

        // $state.go('test');
        $rootScope.product = product;
        $state.go('product',{productId:product.id});
    }
    this.areas = [
        { name: 'ونک' },
        { name: 'میرداماد' },
        { name: 'جردن' },
        { name: 'پارک وی' }
    ];
    $scope.categories = [
        {id:1,title:'خانه',parent:0,image:'cat1.png',bg:'#000',titlebg:'#fff'},
        {id:2,title:'لبنیات',parent:1,image:'cat2.png',bg:'#7a4796',titlebg:'#8652a1'},
        {id:3,title:'دخانیات',parent:1,image:'cat3.png',bg:'#e9ad00',titlebg:'#fbbc05'},
        {id:4,title:'نوشیدنی',parent:1,image:'cat4.png',bg:'#e13b3b',titlebg:'#f04a46'},
        {id:5,title:'تنقلات',parent:1,image:'cat5.png',bg:'#00b520',titlebg:'#16c32f'},
        {id:6,title:'ماست',parent:2,image:'cat1.png',bg:'#7a4796',titlebg:'#8652a1'},
        {id:7,title:'شیر',parent:2,image:'cat2.png',bg:'#7a4796',titlebg:'#8652a1'},
        {id:8,title:'ماست پرچرب',parent:6,image:'cat1.png',bg:'#7a4796',titlebg:'#8652a1'},
        {id:9,title:'ماست کم چرب',parent:6,image:'cat1.png',bg:'#7a4796',titlebg:'#8652a1'},
    ];
    $scope.products = [
        {id:1, title:'شیر کم چرب پگاه',parent:7,image:'3-annas.png', bg:'#7a4796',price:2000,tax:20},
        {id:2, title:'شیر پر چرب پگاه',parent:7,image:'3-annas.png', bg:'#7a4796',price:2000,tax:20},
        {id:3, title:'شیر کم چرب دامداران',parent:7,image:'3-annas.png', bg:'#7a4796',price:2000,tax:10},
        {id:4, title:'پفک چی توز',parent:5,image:'3-annas.png', bg:'#00b520',price:1000,tax:10},
        {id:5, title:'ماست کم چرب پگاه',parent:9,image:'3-annas.png', bg:'#7a4796',price:1500,tax:20},
        {id:6, title:'ماست پر چرب پگاه',parent:8,image:'3-annas.png', bg:'#7a4796',price:1500,tax:20},
        {id:7, title:'ماست پر چرب دامداران',parent:8,image:'3-annas.png', bg:'#7a4796',price:1500,tax:20},
        {id:8, title:'ماست کم چرب دامداران',parent:9,image:'3-annas.png', bg:'#7a4796',price:1500,tax:20},
        {id:9, title:'تست',parent:3,image:'',bg:'#e9ad00',price:20,tax:5},
        {id:9, title:'تست',parent:3,image:'',bg:'#e9ad00',price:20,tax:5},
        {id:9, title:'تست',parent:3,image:'',bg:'#e9ad00',price:20,tax:5},
        {id:9, title:'تست',parent:3,image:'',bg:'#e9ad00',price:20,tax:5},
        {id:9, title:'تست',parent:3,image:'',bg:'#e9ad00',price:20,tax:5}
    ];
    $scope.items = ['item1', 'item2', 'item3'];
    $scope.test = function () {
        alert();
    };
    $scope.animationsEnabled = true;
    $scope.orderList = [
    ]
    $scope.totalNumberOfOrders = 0;
    $rootScope.selectMe = function(data) {
        let obj1 = $scope.orderList.filter(c=> c.pid == data.id);
        if(obj1.length)
        {
            $scope.increaseQuantity(data.id);
        }
        else {
            var tempOrder = {pid: data.id, title: data.title, quantity: 1, price: data.price}
            $scope.orderList.push(tempOrder);
        }
        // mySwiper.update(true);
    };
    $scope.currentDirectory = {title:'خانه',id:1}
    $scope.getSubDirectory = function(cid){
        let obj1 = $scope.categories.filter(c=> c.id == cid);
        $scope.currentDirectory.title = obj1[0].title;
        $scope.currentDirectory.id = obj1[0].id;
        // $scope.swiperUpdate();
    }
    $scope.homeDirectory = function () {
        $scope.currentDirectory.title = 'خانه';
        $scope.currentDirectory.id = 1;
    }
    $scope.backDirectory = function () {
        var cid = $scope.currentDirectory.id
        let obj1 = $scope.categories.filter(c=> c.id == cid);
        cid2 = obj1[0].parent;
        let obj2 = $scope.categories.filter(c=> c.id == cid2);
        $scope.currentDirectory.title = obj2[0].title;
        $scope.currentDirectory.id = obj2[0].id;
        // $scope.swiperUpdate();
    }
    $scope.filterCategoryText = function (text) {
        if(text.length>25)
            return text.substring(0,25) + ' ...';
        else
            return text;
    }
    $scope.filterText = function (text) {
        if(text.length>25)
            return text.substring(0,25) + ' ...';
        else
            return text;
    }
    $scope.increaseQuantity = function(p) {
        let obj1 = $scope.orderList.filter(c=> c.pid == p);
        obj1[0].quantity++;

        $scope.totalNumberOfOrders++;
    };
    $scope.decreaseQuantity = function(p) {
        let obj1 = $scope.orderList.filter(c=> c.pid == p);
        if(obj1[0].quantity>1)
            obj1[0].quantity--;

        $scope.totalNumberOfOrders--;
    };
    $scope.willBeDeleted = -1;
    $scope.deleteMe = function(index) {
        $scope.orderDetailShow = false;
        $scope.willBeDeleted = index;
        $timeout(function () {
            $scope.orderList.splice(index, 1);
            $scope.activeDetail = ($scope.activeDetail>index)?$scope.activeDetail-1:$scope.activeDetail;
            $scope.willBeDeleted = -1;
            // mySwiper.update(true);
        },150);
    };
    $scope.total = function () {
        var total = 0;
        for(var i = 0; i < $scope.orderList.length; i++){
            var order = $scope.orderList[i];
            total += (order.price * order.quantity);
        }
        return total;
    }
    // $scope.total = function() {
    //     var subTotal = $scope.calcSubTotal();
    //     if($scope.discountSwitch)
    //         var total =  subTotal - $scope.discount;
    //     else
    //         var total =  subTotal - (subTotal*parseFloat($scope.discount/100))
    //
    //     if($scope.incompletePayment)
    //         total -= $scope.totalPayments;
    //
    //     return total>0?total:0;
    // }
}
superi.directive('updateTitle', ['$rootScope', '$timeout',
    function($rootScope, $timeout) {
        return {
            link: function(scope, element) {
                var listener = function(event, toState) {

                    var title = 'سوپر مارکت آنلاین';
                    if (toState.data && toState.data.pageTitle) title = toState.data.pageTitle;

                    $timeout(function() {
                        element.text(title);
                    }, 0, false);
                };
                $rootScope.$on('$stateChangeStart', listener);
            }
        };
    }
]);
