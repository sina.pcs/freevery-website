import React from 'react';
import {Link} from 'react-router';
import {ComboBox, Option} from 'belle';
import MyInput from '../common/input';
import ring from '../../../public/images/ring.svg';
import auth from '../../auth';
import axios from 'axios';


class Profile extends React.Component {
    constructor(props,context){
        super(props);
        this.state = {
            first_name: '',
            mobile: '',
            last_name: '',
            email:'',
            address: [],
            pageState: 0,
            id:'',
            validationError:''
        };
        this.handleInputChange=this.handleInputChange.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
    }
    componentWillMount(){
        let user = auth.getUser();
        this.setState({
            first_name:user.first_name?user.first_name:'',
            last_name:user.last_name?user.last_name:'',
            address:user.address?user.address:[],
            email:user.email?user.email:'',
            mobile:user.username,
            id:user.id
        });
    }
    handleSubmit(e){
        e.preventDefault();
        this.setState({
            pageState:1
        })  

        let _this = this;

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + auth.getToken() ;
        
        let objectToSend = {
            "first_name": this.state.first_name,
            "last_name": this.state.last_name,
            "email": this.state.email
        }

        let user = auth.getUser();
        axios.put('https://freevery.com/api/web/user/update',
            objectToSend
        )
            .then((response) => {

                if (response.data.code == '1') {
                    user.first_name = response.data.data.first_name;
                    user.last_name = response.data.data.last_name;
                    user.email = response.data.data.email;
                    auth.updateUser(user);
                    _this.setState({pageState: 0});
                }
                else{
                    _this.setState({
                        pageState: 0,
                        validationError: response.data.message
                    })                    
                }
            })
            .catch(function (error) {
                    _this.setState({
                        pageState: 0,
                        validationError: 'متاسفانه خطایی رخ داد. لطفا مجددا تلاش کنید'
                    })
            });
    }
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }
    render() {
        return (
            <div style={{marginTop: 20}}>
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div className="panel panel-default">
                        <div className="panel panel-default">
                            <div className="panel-heading" role="tab" id="headingOne">
                                <h4 className="panel-title">
                                    <a role="button" data-toggle="collapse" className="collapsed" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        تغییر کلمه عبور
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div className="panel-body">
                                    <PasswordChange />
                                </div>
                            </div>
                        </div>

                        <div className="panel-heading" role="tab" id="headingTwo">
                            <h4 className="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    مشخصات کاربری
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="true">
                            <div className="panel-body">
                                {
                                    this.state.pageState==1?
                                        <div style={{textAlign: 'center'}}>
                                            <object data={ring} type="image/svg+xml">
                                                <img src={ring}/>
                                            </object>
                                        </div>
                                        :
                                        <div>
                                        <ValidationError error={this.state.validationError}/>
                                <form onSubmit={this.handleSubmit}>
                                    <MyInput className="col col-sm-6"
                                             name="first_name"
                                             label="نام"
                                             type="text"
                                             value={this.state.first_name}
                                             onChange={this.handleInputChange}
                                    />
                                    <MyInput className="col col-sm-6"
                                             name="last_name"
                                             label="نام خانوادگی"
                                             type="text"
                                             value={this.state.last_name}
                                             onChange={this.handleInputChange}
                                    />
                                    <MyInput className="col col-sm-6"
                                             name="mobile"
                                             label="تلفن همراه"
                                             type="text"
                                             value={this.state.mobile}
                                    />
                                    <MyInput className="col col-sm-6" 
                                            name="email" 
                                            label="ایمیل" 
                                            type="email"
                                            value={this.state.email}
                                            onChange={this.handleInputChange}
                                            disabled={true}
                                    />

                                    <button type="submit" className="initial-position">
                                        ذخیره
                                    </button>
                                </form>
                                </div>
                                }
                                
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        )
    }
}

class PasswordChange extends React.Component{
    constructor(props, context) {
        super(props);
        this.state = {
            validationError: '',
            successAlert: '',
            pageState: 0, //show form state
            password: '',
            prevPassword: ''
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.changePassword = this.changePassword.bind(this);
    }

    Validation = function (pass1, pass2) {

        let re = /[A-Za-z\d]{8,}/;

        if (!re.test(pass1)) {
            this.setState({
                validationError: 'کلمه عبور حداقل باید 8 کاراکتر داشته باشد',
                signUpState: 0,
            });
            return false;
        }

        if (pass1 != pass2) {
            this.setState({
                validationError: 'کلمه عبور و تکرار کلمه عبور یکسان نیستند',
                signUpState: 0,
            });
            return false
        }

        return true;
    };

    onSubmit(e) {
        e.preventDefault();

        let prevPassword = this.refs.prevPassword.refs.text.value;
        let password = this.refs.password.refs.text.value;
        let passwordConfirm = this.refs.passwordConfirm.refs.text.value;

        if (this.Validation(password, passwordConfirm)) {
            this.setState({
                prevPassword: prevPassword,
                password: password,
                pageState: 1,
                validationError: ''

            },this.changePassword)
        }
    }

    changePassword(){
            let _this = this;
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + auth.getToken() ;
            
            let objectToSend = {
                'pre-password': this.state.prevPassword,
                'password': this.state.password,
            }

            axios.put('https://freevery.com/api/web/user/change-password',
                objectToSend
            )
                .then((response) => {

                    if (response.data.code == '1') {
                        auth.updateToken(response.data.token);
                        _this.setState({pageState: 0,successAlert:'کلمه عبور با موفقیت تغییر یافت'});
                    }
                    else{
                        _this.setState({
                            pageState: 0,
                            validationError: response.data.message
                        })                    
                    }
                })
                .catch(function (error) {
                        _this.setState({
                            pageState: 0,
                            validationError: 'متاسفانه خطایی رخ داد. لطفا مجددا تلاش کنید'
                        })
                });
    }
    render(){
        switch (this.state.pageState) {
            case 0 :
                return (
                    <div>
                        {this.state.successAlert != '' &&
                            <div style={{marginBottom: 40, color:'green'}}>{this.state.successAlert}</div>
                        }
                        <ValidationError error={this.state.validationError}/>
                        <form onSubmit={this.onSubmit}>
                            <MyInput name="prevPassword" ref="prevPassword" label="کلمه عبور قبلی"
                                     type="password"/>
                            <MyInput name="password" ref="password" label="کلمه عبور (حداقل 8 کاراکتر)"
                                     type="password"/>
                            <MyInput name="passwordConfirm" ref="passwordConfirm" label="تکرار کلمه عبور"
                                     type="password"/>

                            <button type="submit" className="initial-position">
                                ذخیره
                            </button>
                        </form>
                    </div>
                );

            case 1 :
                return (
                   
                    <div style={{textAlign: 'center'}}>
                        <object data={ring} type="image/svg+xml">
                            <img src={ring}/>
                        </object>
                    </div>
                )
        }
    }
}
class ValidationError extends React.Component {
    render() {
        if (this.props.error != '') {
            return (
                <div className="alert alert-danger" style={{marginBottom: 50,marginBottom: 40}}>
                    {this.props.error}
                </div>
            )
        }
        else
            return <div></div>
    }
}
export default Profile;