import React, {Component} from 'react';
import OneOrderInList from '../common/OneOrderInList';
import ring from '../../../public/images/ring.svg';
import auth from '../../auth';
import axios from 'axios';

class Order extends Component {
    constructor(props, context) {
        super(props);
        this.state = {
            order: {},
            pageState:0
        };
        this.makeView = this.makeView.bind(this);
    }
    componentWillMount(){
        let oid = this.props.params.oid;
        let _this = this;
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + auth.getToken() ;
        axios.get('https://freevery.com/api/web/orders/get-by-orderid?order_id='+oid)
            .then(function (response) {
                if(response.data.code="1")
                {
                    _this.setState({
                        pageState:1,
                        order:response.data.data[0]
                    })
                }
            })
            .catch(function (error) {
            })
    }
    makeView(){
        this.rows = [];
        let totalNumberOfOrders = 0;

        function total() {
            return 0;
        }

        if(this.state.order.orderDetail){
            for(let i = 0; i < this.state.order.orderDetail.length ; i++){
                this.rows.push(<OneOrderInList editable={false} order={this.state.order.orderDetail[i].product[0]} key={'o'+i}/>)
            }
        }
    }
    render() {
        let rows = [];
        let orderStatus = '';
        let totalNumberOfOrders = 0;
        let button = '';

        if(this.state.order.orderDetail){
            for(let i = 0; i < this.state.order.orderDetail.length ; i++){
                let order = this.state.order.orderDetail[i].product[0];
                order.price = this.state.order.orderDetail[i].price;
                order.quantity = this.state.order.orderDetail[i].count;
                rows.push(<OneOrderInList editable={false} order={order} key={'o'+i}/>)
            }

            switch (this.state.order.state){
                case '0':
                    orderStatus = 'در انتظار تایید سوپرمارکت'
                    break;
                case '1':
                    orderStatus = 'در انتظار تایید شما';
                    button = <button className="primary-btn">تایید سفارش</button>;
                    break;
                case '2':
                    orderStatus = 'در انتظار تایید شما';
                    button = <button className="primary-btn">تایید سفارش</button>;
                    break;
                case '3':
                    orderStatus = 'پرداخت ناموفق'
                    break;
                case '4':
                    orderStatus = 'تایید کاربر'
                    break;
                case '5':
                    orderStatus = 'در راه'
                    break;
                case '6':
                    orderStatus = 'تحویل داده شد'
                    break;
                case '7':
                    orderStatus = 'بسته شد'
                    break;

            }
        }




        if(this.state.pageState == 0)
        {
            return(
                <div style={{textAlign: 'center'}}>
                    <object data={ring} type="image/svg+xml">
                        <img src={ring}/>
                    </object>
                </div>
            )
        }
        else {
            return (
            <div style={{padding:20}}>
                <div className="supplier-status">
                    <span className="glyphicon glyphicon-time"/>{orderStatus}
                </div>
                <div id="orderList" className="not-editable">
                    <div id="orders">
                        <div className="order">
                            <div className="title">نام محصول</div>
                            <div className="qauntity">تعداد</div>
                            <div className="price">قیمت واحد</div>
                        </div>
                        {rows}
                    </div>
                </div>
                {button}
            </div>
            )
        }
    }
}


export default Order;
