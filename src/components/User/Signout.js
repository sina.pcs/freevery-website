import React, { Component } from 'react';
import auth from '../../auth';

class Signout extends Component {
    componentDidMount() {
        auth.logout();
        this.props.router.replace('/login');
    }

    render() {
        return <p style={{textAlign:'center',marginTop:50,fontSize:22}}>شما با موفقیت خارج شدید</p>
    }
}

export default Signout;
