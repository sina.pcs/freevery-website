import React from 'react';
import {Link} from 'react-router';
import {ComboBox, Option} from 'belle';
import MyInput from '../common/input';
import axios from 'axios';
import auth from '../../auth';
import ring from '../../../public/images/ring.svg';
const jalaali = require('jalaali-js');

class OrdersHistoryPage extends React.Component {
    constructor(props,context){
        super(props);
        this.state = {
            pageState:0,
            orders:[]
        };
    }
    componentWillMount(){
        let user = auth.getUser();
        let _this = this;
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + auth.getToken() ;
        axios.get('https://freevery.com/api/web/orders/get-order-by-customer-id-and-state?customer_id='+user.id)
            .then(function (response) {
                if(response.data.code="1")
                {
                    _this.setState({
                        pageState:1,
                        orders:response.data.data
                    })
                }
            })
            .catch(function (error) {

            })
    }
    render() {
        if(this.state.pageState == 0)
        {
            return(
                <div style={{textAlign: 'center',marginTop:40}}>
                    <object data={ring} type="image/svg+xml">
                        <img src={ring}/>
                    </object>
                </div>
            )
        }
        else{
            let orders = this.state.orders.map((order) => {
                let t = order.insert_datetime.split(/[- :]/);
                let j = jalaali.toJalaali(parseInt(t[0]), parseInt(t[1]), parseInt(t[2]));
                let orderStatus = '';
                switch (order.state) {
                    case '-1':
                        orderStatus = 'لغو شده'
                        break;
                    case '0':
                        orderStatus = 'در انتظار تایید سوپرمارکت'
                        break;
                    case '1':
                        if(order.payment_method == "1"){
                            orderStatus = 'در انتظار پرداخت آنلاین';
                        }
                        else{
                            orderStatus = 'سفارش شما تایید شد';
                        }
                        break;
                    case '2':
                        if(order.payment_method == "1"){
                            orderStatus = 'در انتظار تایید شما و پرداخت آنلاین';
                        }
                        else {
                            orderStatus = 'در انتظار تایید شما';
                        }
                        break;
                    case '3':
                        orderStatus = 'پرداخت ناموفق';
                        break;
                    case '4':
                        if(order.payment_method == "1"){
                            orderStatus = 'پرداخت موفق';
                        }
                        else{
                            orderStatus = 'تایید شده توسط شما';
                        }
                        break;
                    case '5':
                        orderStatus = 'در راه'
                        break;
                    case '6':
                        orderStatus = 'تحویل داده شد'
                        break;
                    case '7':
                        orderStatus = 'بسته شد'
                        break;

                }
                return <Link to={`/order-status/`+order.id} className="list-group-item" key={'order'+order.id}>
                    <div style={{float: 'right',paddingLeft: 20}}>{j.jy + '/' + j.jm + '/' + j.jd}</div>
                    سفارش #{order.id}
                    <div style={{float:'left'}}> {orderStatus}</div>
                </Link>
            });
            return (
                <div style={{marginTop: 20}}>
                    <div className=" panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">سابقه سفارشات</h3>
                        </div>
                        <div className="panel-body">
                            <div className="list-group">
                                {orders}
                            </div>
                        </div>
                    </div>
                </div>

            )

        }
    }
}

export default OrdersHistoryPage;