import React from 'react';
import CheckoutForm from '../common/CheckoutForm';
import CheckoutPageHeader from '../common/CheckoutPageHeader';
import MobileVerificationPage from '../main/MobileVerificationPage';
import auth from '../../auth';

class CheckoutPage extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {
            pageState: 0
        };
    }
    componentWillMount(){
        if(auth.loggedIn())
        {
            this.setState({
                pageState: 1
            });
        }
    }
    render() {
        return(

            <div style={{marginTop: 20}} className="checkout">
                {/*{this.state.pageState<2 && <CheckoutPageHeader active={this.state.pageState} />}*/}
                {this.state.pageState == 0 && <MobileVerificationPage changeState={()=>this.setState({pageState:1})}/>}
                {this.state.pageState == 1 && <CheckoutForm changeState={(orderId)=>this.props.router.push('/order-status/'+orderId)} />}

            </div>
        );
    }
}

export default CheckoutPage;