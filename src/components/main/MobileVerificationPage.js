import React from 'react';
import {Link} from 'react-router';
import {ComboBox, Option} from 'belle';
import MyInput from '../common/input';
import Login from '../common/Login';
import MobileVerification from '../common/MobileVerification';
import { Steps, Hints } from 'intro.js-react';
import 'intro.js/introjs.css';
import 'intro.js/introjs-rtl.css';

class MobileVerificationPage extends React.Component {
    constructor(props,context){
        super(props);
        this.state = {
            stepsEnabled: false,
            initialStep: 0,
            steps: [
                {
                    element: '#login-before-checkout',
                    intro: '<h3>ورود به سایت</h3><div>برای ثبت سفارش باید وارد ناحیه کاربری خود شوید.</div>',
                    position: 'right'
                },

                {
                    element: '#signup-btn-in-login-form',
                    intro: '<h3>عضویت</h3><div>در صورتی که قبلا در سایت ثبت نام نکرده اید بر روی دکمه عضویت کلیک کنید.</div>',
                    position: 'right'
                }
            ]
        };
    }

    onExit = () => {
        localStorage.setItem('login-intro','seen');
        this.setState(() => ({ stepsEnabled: false }));
    };

    componentDidMount(){
        document.title = "Freevery - ورود/ثبت نام";
    }
    componentWillMount(){
        if(localStorage.getItem('login-intro') !== 'seen' && window.innerWidth > 991) {
            this.setState(() => ({stepsEnabled: true}));
        }
    }
    render() {
        return (
            <div style={{marginTop: 20}} className="mobile-verification row">
                <Steps
                    enabled={this.state.stepsEnabled}
                    steps={this.state.steps}
                    initialStep={this.state.initialStep}
                    onExit={this.onExit}
                    options={
                        {
                            nextLabel: 'بعدی',
                            prevLabel: 'قبلی',
                            skipLabel: 'پرش',
                            doneLabel: 'اتمام'
                        }
                    }
                />
                {/*<div className="col col-sm-12">*/}
                    {/*<div className="alert alert-info" role="alert"*/}
                         {/*style={{width: '100%', borderRadius: 0}}>*/}
                        {/*در صورتی که قبلا در سایت ثبت نام کرده اید لطفا از فرم اول استفاده کرده و وارد شوید.*/}
                        {/*اما در صورتی که قبلا در سایت ثبت نام نکرده اید شماره همراه خود را در فرم دوم وارد کنید.*/}
                    {/*</div>*/}
                {/*</div>*/}
                <div id="login-before-checkout" className="col col-sm-6 col-sm-offset-3">
                    <div className=" panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">ورود به سایت</h3>
                        </div>
                        <div className="panel-body">
                            <Login/>
                        </div>
                        <Link id="signup-btn-in-login-form" className="btn btn-default" style={{margin:10}}  activeClassName="active" to="/signup">
                            <span className="glyphicon glyphicon-edit"/> عضویت
                        </Link>
                    </div>
                </div>
                {/*<div className="col col-sm-6">*/}
                    {/*<div className=" panel panel-default">*/}
                        {/*<div className="panel-heading">*/}
                            {/*<h3 className="panel-title">ثبت نام</h3>*/}
                        {/*</div>*/}
                        {/*<div className="panel-body">*/}
                            {/*<MobileVerification changeState={()=>this.props.changeState()} />*/}
                        {/*</div>*/}
                    {/*</div>*/}
                {/*</div>*/}
            </div>

        )
    }
}

export default MobileVerificationPage;