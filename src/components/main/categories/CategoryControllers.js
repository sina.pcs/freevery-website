import React from 'react';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as orderActions from '../../../actions/orderActions';
import MyInput from '../../common/input';
import axios from 'axios';
import ProductButton from './ProductButton';

let timeout = null;
class CategoriesController extends React.Component{
    constructor(props,context){
        super(props);
        this.state = {
            result: '',
            searched: 0
        };
        this.search=this.search.bind(this);
        this.addThis=this.addThis.bind(this);
    }
    goBack = function (e) {
        window.history.back();
    };

    addThis(e, product) {
        e.preventDefault();
        let orderIndex = this.props.orders.findIndex(order => order.pid == product.id);
        if (orderIndex != -1) {
            this.props.actions.increaseOrder(orderIndex);
        }
        else {
            let order = {pid: product.id, title: product.title, quantity: 1, price: product.price, categoryId:product.category_id};
            this.props.actions.addOrder(order);
        }

        this.props.actions.increaseTotal(product.price);
    }
    search(event){
        clearTimeout(timeout);
        let value =event.target.value;
        timeout = setTimeout(() => {
            if(value.length > 2) {
                this.setState({
                    searched : 1
                });
                let _this = this;
                axios.post('https://freevery.com/api/web/product/advanced-search',
                    {
                        "query": {
                            "bool": {
                                "should": [
                                    {
                                        "match": {
                                            "title": value
                                        }
                                    },


                                    {
                                        "prefix": { "title" :  { "value" : value, "boost" : 2.0 } }
                                    }
                                ]
                            }
                        },
                        "from" : 0, "size" : 1000
                    }

                )
                    .then(function (result) {

                        _this.setState({
                            result: JSON.parse(result.data).hits.hits,
                            searched: 2
                        })

                    })
            }
            else{
                this.setState({
                    result: null,
                    searched:0
                })
            }
        }, 500);
    }
    render() {
        let results = '';
        switch(this.state.searched){
            case 0:
                results = '';
                break;

            case 1:
                results = <div>در حال جستجو</div>;
                break;

            case 2:

                results = this.state.result.map((o,i)=>{
                    let product = o._source;
                    return <ProductButton addThis={(e)=> this.addThis(e, product)}
                                          product={product} key={'r'+product.id}/>
                });
                break;
        }

        return (
            <div className="main-search">
                <MyInput className="search" name="search" onChange={this.search} type="text" label="جستجو محصول یا دسته مورد نظر" />
                {results != '' && <div className="search-result">
                    {results}
                    <div className="clear" />
                </div>}
                {/*<div id="products-navigation-bar">*/}
                    {/*<div className="col-md-2 col-xs-6">*/}
                        {/*<Link to="/" className="btn">خانه</Link>*/}
                    {/*</div>*/}
                    {/*<div className="col-md-2  col-xs-6">*/}
                        {/*<a className="btn" onClick={this.goBack}>بازگشت</a>*/}
                    {/*</div>*/}
                    {/*<div className="col-md-7 col-xs-12 currentDirectory">*/}
                        {/*شاخه فعلی : {this.props.thisCategoryTitle}*/}
                    {/*</div>*/}
                {/*</div>*/}
            </div>
        )
    }
}
function mapStateToProps(state, ownProps) {
    return {
        //orders: Object.keys(state.orders).map(function (key) { return state.orders[key]; })
        orders: state.orders
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(orderActions, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CategoriesController);