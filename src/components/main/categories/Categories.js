import React from 'react';
import CategoryButton from './CategoryButton';

class Categories extends React.Component {
    render() {
        let thisCategory = this.props.cid;
        let categories = this.props.categories;
        function getCategories() {
            return categories.map(category => <CategoryButton category={category} key={category.id}/>);
        }

        return (
            <div id="products">
                {getCategories()}
            </div>
        );
    }
}

export default Categories;