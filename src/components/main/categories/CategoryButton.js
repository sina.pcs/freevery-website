import React from 'react';
import {Link} from 'react-router';

class CategoryButton extends React.Component {
    render() {
        let category = this.props.category;

        const buttonStyle = {
            height: 141,
            paddingTop: 15,
            boxSizing: 'border-box',
            //background: 'url('+require('../../../../public/images/products/'+category.image)+') center center no-repeat'
        };

        function filterText(text) {
            if(text.length>40)
                return text.substring(0,40) + ' ...';
            else
                return text;
        }

        return (
            <div className="product-container">
                <Link to={`/category/${category.id}`} className="md-raised product-button">
                    <div>
                        <div style={buttonStyle}></div>
                        <div className={"title " + (category.title.length>24 && 'twolineCategory')}>
                            {filterText(category.title)}
                        </div>
                    </div>
                </Link>
            </div>
        );
    }
}

export default CategoryButton;