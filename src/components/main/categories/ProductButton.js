import React from 'react';
import {Link} from 'react-router';
import ring from '../../../../public/images/ring.svg';

class ProductButton extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            imageLoaded:0
        };
    }
    render() {
        let product = this.props.product;
        let imageName = this.props.product.image_name?this.props.product.image_name:this.props.product.code+'.jpg';
        // const buttonStyle = {
        //     background: 'url(/images/products/'+imageName+') center center no-repeat',
        //     backgroundSize: 'contain',
        // };
        function filterText(text) {
            if(text.length>40)
                return text.substring(0,40) + ' ...';
            else
                return text;
        }
        return (
                <div className='product-container'>
                    <div className="product-button">
                        <div style={{position: 'relative', width: 100+'%', height: 100+'%', overflow: 'hidden'}}>
                            <div className="title-container">
                                {this.state.imageLoaded===0 && <img src={ring} role="presentation"  />}
                                {this.state.imageLoaded===2 && <img className="default-image" src={'/images/freevery.png'} role="presentation"  />}
                                <img src={'/images/products/thumbnail/'+imageName} alt={product.title} className={this.state.imageLoaded !== 1?'hidden':''} onLoad={()=>{this.setState({imageLoaded:1})}} onError={()=>{this.setState({imageLoaded:2})}} />
                            </div>
                            <div className="title" style={{display:'table'}}>
                                <div style={{display:'table-cell',verticalAlign:'middle'}}>
                                    {filterText(product.title)}
                                </div>
                            </div>
                            <div className="title">
                                {product.price} تومان
                            </div>
                            <div className="detail-buttons">
                                <Link to={`/product/${product.id}/${product.title}`}><span className="glyphicon glyphicon-search"/></Link>
                                <a href="#" onClick={this.props.addThis}>
                                    <span className="glyphicon glyphicon-plus"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}

export default ProductButton;