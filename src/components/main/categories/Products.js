import React from 'react';
import Request from 'react-http-request';
import ProductButton from './ProductButton';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as orderActions from '../../../actions/orderActions';
import ring from '../../../../public/images/ring.svg';
import axios from 'axios';

class Products extends React.Component {
    constructor(props, context) {
        super(props);
        this.state = {
            products: [],
            loading: false,
            sortType: 'newest'
        };
        this.addThis = this.addThis.bind(this);
        this.getProducts = this.getProducts.bind(this);
        this.handleSort = this.handleSort.bind(this);
    }

    addThis(e, product) {
        e.preventDefault();
        let loadingAnimation = document.createElement("div");
        let productTitle = document.createTextNode(product.title + ' به سبد خرید اضافه شد');
        loadingAnimation.appendChild(productTitle);
        loadingAnimation.className = 'fadeOutLeft animated product-animation';
        loadingAnimation.style.top = e.pageY + 'px';
        loadingAnimation.style.left = e.pageX - 200 + 'px';
        let currentDiv = document.getElementById("root");
        document.body.insertBefore(loadingAnimation, currentDiv);

        let orderIndex = this.props.orders.findIndex(order => order.pid == product.id);
        if (orderIndex != -1) {
            this.props.actions.increaseOrder(orderIndex);
        }
        else {
            let order = {
                pid: product.id,
                title: product.title,
                quantity: 1,
                price: product.price,
                categoryId: product.category_id
            };
            this.props.actions.addOrder(order);
        }

        this.props.actions.increaseTotal(product.price);
    }

    componentDidMount() {
        if (this.props.cid == 1)
            document.title = "Freevery - سوپرمارکت اینترنتی فریوری";
        else
            document.title = "Freevery - " + this.props.ctitle;
    }

    componentWillMount() {
        this.getProducts(this.props.cid);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.cid != this.props.cid)
            this.getProducts(nextProps.cid);

        if (nextProps.cid == 1)
            document.title = "Freevery - سوپرمارکت اینترنتی فریوری";
        else
            document.title = "Freevery - " + nextProps.ctitle;
    }

    handleSort(event) {
        let selectedValue = event.target.value;
        this.setState({sortType: selectedValue, loading: true});

        let sorted = this.state.products.slice(0);

        switch (selectedValue) {
            case 'title':
                sorted.sort(function (a, b) {
                    let x = a.title.toLowerCase();
                    let y = b.title.toLowerCase();
                    return x < y ? -1 : x > y ? 1 : 0;
                });
                break;
            case 'newest':
                sorted.sort(function (a, b) {
                    return parseInt(a.id) - parseInt(b.id);
                });
                break;

            case 'price':
                sorted.sort(function (a, b) {
                    return parseInt(a.price) - parseInt(b.price);
                });
                break;

            case 'priceDesc':
                sorted.sort(function (a, b) {
                    return parseInt(b.price) - parseInt(a.price);
                });
                break;
        }
        this.setState({
            products: sorted,
            loading: false
        })
    }

    getProducts(cid) {
        if (cid == 1) {
            axios.get('https://freevery.com/api/web/product/get-featured')
                .then((result) => {
                    this.setState({
                        products: result.data.data,
                        loading: false
                    })

                })
                .catch((error) => {
                    this.getProducts();
                });
            return;
        }
        this.setState({
            loading: true
        });
        axios.get('https://freevery.com/api/web/product/get-by-catid?cat_id=' + cid)
            .then((result) => {
                this.setState({
                    products: result.data.data,
                    loading: false
                })

            })
            .catch((error) => {
                this.getProducts();
            });
        // let products_url = 'https://freevery.com/api/web/product/get-by-catid?cat_id='+this.props.cid;
        // return (
        //     <Request
        //         url={products_url}
        //         method='get'
        //         accept='application/json'
        //     >
        //         {
        //             ({error, result, loading}) => {
        //                 if (loading) {
        //                     return (
        //                         <div style={{textAlign:'center'}}>
        //                             <object data={ring} type="image/svg+xml">
        //                                 <img src={ring}/>
        //                             </object>
        //                         </div>
        //                     );
        //                 } else {
        //                     return (
        //                         <div>
        //                             <select value={this.state.sortType} onChange={this.handleSort}>
        //                                 <option value="newest">
        //                                     جدیدترین ها
        //                                 </option>
        //                                 <option value="price">
        //                                     قیمت کم به زیاد
        //                                 </option>
        //                                 <option value="priceDesc">
        //                                     قیمت زیاد به کم
        //                                 </option>
        //                             </select>
        //                             { result.body.data.map(product => <ProductButton addThis={(e)=> this.addThis(e, product)}
        //                                                                         product={product} key={product.id}/>) }
        //                         </div>
        //                     );
        //                 }
        //             }
        //         }
        //     </Request>
        // );
    }

    render() {
        if (this.state.loading)
            return (
                <div style={{textAlign: 'center'}}>
                    <object data={ring} type="image/svg+xml">
                        <img src={ring}/>
                    </object>
                </div>
            );
        else if (this.props.cid !== 1)
            return (
                <div id="products">
                    {this.props.ctitle ? <div className="category-title">{this.props.ctitle}</div> : <div></div>}
                    <div className="sort-select">
                        <label htmlFor="sort" style={{marginLeft: 10}}>مرتب سازی بر اساس</label>
                        <select id="sort" value={this.state.sortType} onChange={this.handleSort}>
                            <option value="newest">
                                جدیدترین ها
                            </option>
                            <option value="title">
                                حروف الفبا
                            </option>
                            <option value="price">
                                قیمت کم به زیاد
                            </option>
                            <option value="priceDesc">
                                قیمت زیاد به کم
                            </option>
                        </select>
                    </div>
                    <div>
                        {this.state.products.map(product =>{
                            if(product.price > 100)
                                return(
                                    <ProductButton
                                        addThis={(e) => this.addThis(e, product)}
                                        product={product} key={product.id}/>)
                            })
                        }
                        <div className="clear"/>
                    </div>
                </div>
            );

        else
            return (
                <div>
                <div className="row services">
                    <div className="col col-md-3 col-sm-6">
                        <img src={require('../../../../public/images/delivery.png')} />
                         ارسال رایگان سفارش
                        <br/>
                        در کمتر از 1 ساعت
                    </div>
                    <div className="col col-md-3 col-sm-6">
                        <img src={require('../../../../public/images/payments.png')} />
                       پرداخت آنلاین
                        <br/>
                        یا پرداخت در محل
                    </div>
                    <div className="col col-md-3 col-sm-6">
                        <img src={require('../../../../public/images/clock.png')} />
ساعت کاری فریوری
                        <br/>
                        8 صبح تا 10 شب
                    </div>
                    <div className="col col-md-3 col-sm-6">
                        <img src={require('../../../../public/images/tehran.png')} />
                        پوشش منطقه 1 تهران
                        <br/>
                        و به زودی سایر مناطق
                    </div>
                </div>
                    <div id="products">
                        <div className="category-title">محصولات منتخب</div>
                        <div className="sort-select">
                            <label htmlFor="sort" style={{marginLeft: 10}}>مرتب سازی بر اساس</label>
                            <select id="sort" value={this.state.sortType} onChange={this.handleSort}>
                                <option value="newest">
                                    جدیدترین ها
                                </option>
                                <option value="title">
                                    حروف الفبا
                                </option>
                                <option value="price">
                                    قیمت کم به زیاد
                                </option>
                                <option value="priceDesc">
                                    قیمت زیاد به کم
                                </option>
                            </select>
                        </div>
                        <div>
                            {this.state.products.map(product =>
                                <ProductButton
                                    addThis={(e) => this.addThis(e, product)}
                                    product={product} key={product.id}/>) }
                            <div className="clear"/>
                        </div>
                    </div>
                </div>
            )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        //orders: Object.keys(state.orders).map(function (key) { return state.orders[key]; })
        orders: state.orders
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(orderActions, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Products);
// export default Products;