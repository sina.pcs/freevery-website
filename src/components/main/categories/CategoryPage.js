import React from 'react'
import CategoryControllers from './CategoryControllers';
import Products from './Products';
import { Steps, Hints } from 'intro.js-react';
import 'intro.js/introjs.css';
import 'intro.js/introjs-rtl.css';

class CategoryPage extends React.Component{

    constructor(props,context){
        super(props);
        this.state = {
            stepsEnabled: false,
            initialStep: 0,
            steps: [

                {
                    element: '.header-buttons',
                    intro: '<h3>ناحیه کاربری</h3><div>برای ثبت سفارش و استفاده از سایر مزایای فریوری می بایست در سایت ثبت نام کنید و یا وارد حساب کاربری شوید</div>',
                    position: 'right'
                },
                {
                    element: '.nav li',
                    intro: '<h3>دسته بندی محصولات</h3><div>منو اصلی دسته بندی محصولات در این قسمت قرار دارد و شما می توانید دسته های مختلف را از این قسمت انتخاب کنید. </div>',
                    position: 'bottom-middle-aligned'
                },
                {
                    element: '.main-search',
                    intro: '<h3>جستجو در محصولات</h3><div>با وارد کردن بخشی از نام محصول مورد نظر خود در این کادر می توانید جستجو را انجام دهید. برای شروع جستجو حداقل 3 حرف اول را باید وارد کنید. </div>',
                },
                {
                    element: '#orderList',
                    intro: '<h3>سبد خرید</h3><div>این قسمت سبد خرید شماست و محصولات انتخابی شما در این قسمت قرار می گیرند</div>',
                    position: 'right'
                }
            ]
        };
    }

    onExit = () => {
        localStorage.setItem('main-intro', 'seen');
        this.setState(() => ({ stepsEnabled: false }));
    };

    componentWillReceiveProps(nextProps){
    }
    componentWillMount(){
        if(localStorage.getItem('main-intro') !== 'seen' && window.innerWidth > 991) {
            if (!this.props.params || !this.props.params.cid)
                this.setState(() => ({stepsEnabled: true}));
        }
    }
    render(){
        let thisCategory = 1;
        let thisCategoryTitle = '';

        if(this.props.params && this.props.params.cid){
            thisCategory = this.props.params.cid;
            thisCategoryTitle = this.props.params.cname;
        }

        function findById(element) {
            return element.id == thisCategory;
        }

        function getCategoriesOrProducts() {
            //let categories = categoriesData.filter(category => category.parent == thisCategory);
            //if(categories.length == 0) {
                return (<Products cid={thisCategory} ctitle={thisCategoryTitle} />);
            //}
            //else
                //return (<Categories categories={categories} />);
        }



        return (
            <div>
                <Steps
                    enabled={this.state.stepsEnabled}
                    steps={this.state.steps}
                    initialStep={this.state.initialStep}
                    onExit={this.onExit}
                    options={
                        {
                            nextLabel: 'بعدی',
                            prevLabel: 'قبلی',
                            skipLabel: 'پرش',
                            doneLabel: 'اتمام'
                        }
                    }
                />
                {/*<NavigationMenu />*/}
                <CategoryControllers thisCategoryTitle={thisCategoryTitle} />
                {getCategoriesOrProducts()}
            </div>
        )
    }
}

export default CategoryPage;
