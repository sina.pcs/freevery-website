import React from 'react';
import Request from 'react-http-request';
import PopupTemplate from '../common/PopupTemplate';
import SignUp from '../common/SignUp';

class SignUpPage extends React.Component {
    render() {
        return (
            <PopupTemplate>
                <div className="popup-header">
                    <span className="title"><span className="glyphicon glyphicon-edit"/> ثبت نام در سایت</span>
                </div>
                <div className="popup-main-content">
                    <SignUp />
                </div>
            </PopupTemplate>
        );
    }
}

export default SignUpPage;