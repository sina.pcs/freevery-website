import React from 'react';
import PopupTemplate from '../common/PopupTemplate';

const ContactPage = () => {
    return (
        <PopupTemplate>
            <div className="popup-header">
                <span className="title">تماس با ما</span>
            </div>
            <div className="popup-main-content about">
                برای تماس با ما می توانید از راه های زیر استفاده کنید.
                <ul>
                    <li>
                        <strong>شماره تماس :</strong>
                        021-44148536
                    </li>
                    <li>
                        <strong>ایمیل :</strong>
                         info@freevery.com
                    </li>
                    <li>
                        <strong>آدرس :</strong>

                        بلوار تعاون - خیابان رحمانی - خیابان لاله - نبش کوچه غلامی - پلاک 27 - واحد 3
                    </li>
                    <li>
                        <strong>کدپستی :</strong>

                        14867786513
                    </li>
                </ul>
            </div>
        </PopupTemplate>
    );
};

export default ContactPage;