import React from 'react';
import PopupTemplate from '../common/PopupTemplate';
import OrdersList from '../common/OrdersList';


class OrdersInMobile extends React.Component {
    render() {
        return (
            <PopupTemplate>
                <div className="popup-header">
                    <span className="title"><span className="glyphicon glyphicon-shopping-cart"/> سبد خرید</span>
                </div>
                <div className="popup-main-content" style={{padding:'0 0 80px'}}>
                    <OrdersList/>
                </div>
            </PopupTemplate>
        );
    }
}
export default OrdersInMobile;