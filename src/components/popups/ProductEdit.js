import React from 'react';
import ring from '../../../public/images/ring.svg';
import auth from '../../auth';
import axios from 'axios';

class ProductEdit extends React.Component {
    constructor(props,context){
        super(props);
        this.state = {
            price : '',
            title : '',
            code : '',
            category_id : '',
            description : '',
            image_name: '',
            imageUploadStatus:''
        }
        this.updateProduct = this.updateProduct.bind(this);
        this.deleteProduct = this.deleteProduct.bind(this);
        this.setImage = this.setImage.bind(this);
    }
    componentWillMount(){
        if(this.props.product != 'new')
        this.setState({
            price:this.props.product.price,
            title:this.props.product.title,
            code:this.props.product.code,
            category_id:this.props.product.category_id,
            description:this.props.product.description?this.props.product.description:'',
            image_name:this.props.product.image_name
        })
    }
    setImage(file){
        var data = new FormData();
        data.append('fileToUpload', file);
        var config = {
            onUploadProgress: function(progressEvent) {
                var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
            }
        };
        let _this = this;

        _this.setState({
            imageUploadStatus:'در حال آپلود'
        });

        axios.post('https://freevery.com/upload.php', data, config)
            .then(function (res) {
                if(res.data.status == 1) {
                    _this.setState({
                        image_name: res.data.message,
                        imageUploadStatus: 'عکس با موفقیت آپلود شد'
                    });
                }
                else{
                    _this.setState({
                        imageUploadStatus:res.data.message
                    });
                }
            })
            .catch(function (err) {
                _this.setState({
                    imageUploadStatus:'خطا در آپلود عکس'
                });
            });
    }
    updateProduct(e){
        e.preventDefault();

        if(this.props.product == 'new'){
            let product = {};
            product.price = this.state.price;
            product.title = this.state.title;
            product.code = this.state.code;
            product.category_id = this.state.category_id;
            product.description = this.state.description;
            product.image_name = this.state.image_name;

            axios.post('https://freevery.com/api/web/product/create',
                product
            )
                .then(function (result) {
                    alert('اضافه شد');
                })
                .catch(function (error) {
                    alert('failed');
                })

        }
        else {
            let product = this.props.product;
            product.price = this.state.price;
            product.title = this.state.title;
            product.code = this.state.code;
            product.category_id = this.state.category_id;
            product.description = this.state.description;
            product.image_name = this.state.image_name;

            axios.put('https://freevery.com/api/web/product/update?id=' + product.id,
                product
            )
                .then(function (result) {
                    alert('آپدیت شد');
                })
                .catch(function (error) {
                    alert('failed');
                })
        }
    }
    deleteProduct(){
        let r = confirm("مطمئن؟");
        if (r == true) {
            let product = this.props.product;

            axios.get('https://freevery.com/api/web/product/delete-by-id?id=' + product.id)
                .then(function (result) {
                    alert('حذف شد');
                })
                .catch(function (error) {
                    alert('failed');
                })
        } else {
            return
        }


    }
    render() {

            return (
                <div>
                    <form onSubmit={(event) => this.updateProduct(event)} className="edit-product-form">
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <label htmlFor="name">نام : </label>
                                    </td>
                                    <td>
                                        <input id="name" type="text" onChange={(event)=>{this.setState({title:event.target.value})}} value={this.state.title} />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label htmlFor="price">قیمت : </label>
                                    </td>
                                    <td>
                                        <input id="price" type="text" onChange={(event)=>{this.setState({price:event.target.value})}} value={this.state.price} />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label htmlFor="code">بارکد : </label>
                                    </td>
                                    <td>
                                        <input id="code" type="text" onChange={(event)=>{this.setState({code:event.target.value})}} value={this.state.code} />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label htmlFor="category_id">دسته : </label>
                                    </td>
                                    <td>
                                        <input id="category_id" type="text" onChange={(event)=>{this.setState({category_id:event.target.value})}} value={this.state.category_id} />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label htmlFor="description">توضیحات : </label>
                                    </td>
                                    <td>
                                        <textarea id="description" type="text" onChange={(event)=>{this.setState({description:event.target.value})}} value={this.state.description} />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>عکس : </label>
                                    </td>
                                    <td>
                                        <ImageUpload setImage={this.setImage} data={this.state.image_name?this.state.image_name:this.state.code+'.jpg'} status={this.state.imageUploadStatus} />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="submit" value="تایید" />
                    </form>
                    <button onClick={this.deleteProduct} style={{background:'#ff6060', color:'#fff',marginTop:10,padding:'10px 20px'}}>حذف</button>

            </div>
    )
    }
}


class ImageUpload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {file: '',imagePreviewUrl: '',imageUrl:''};
    }

    componentDidMount(){
        this.setState({
            imageUrl:this.props.data
        });
    }
    _handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
            this.props.setImage(file);
        };

        reader.readAsDataURL(file)
    }

    render() {
        let {imagePreviewUrl} = this.state;
        let $imagePreview = null;
        if (imagePreviewUrl) {
            $imagePreview = (<img src={imagePreviewUrl} />);
        } else {
            if(this.state.imageUrl)
                $imagePreview = (<img style={{width:'100%'}} src={'/images/products/'+this.state.imageUrl} />);
            else
                $imagePreview = (<div className="previewText">پیش نمایش عکس</div>);
        }

        return (
            <div className="previewComponent" style={{color:'#fff'}}>
                <input className="fileInput" type="file" onChange={(e)=>this._handleImageChange(e)} />
                <div className="imgPreview" style={{marginTop:10,position:'relative',width:150}}>
                    {$imagePreview}
                    {this.props.status &&
                    <div style={{
                        width: '100%',
                        height: '100%',
                        paddingTop: 20,
                        position: 'absolute',
                        top: 0,
                        right: 0,
                        background: 'rgba(0,0,0,0.3)',
                        textAlign: 'center'
                    }}>
                        {this.props.status}
                    </div>
                    }
                </div>
            </div>
        )
    }
}

export default ProductEdit;