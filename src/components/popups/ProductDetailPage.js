import React from 'react';
import Request from 'react-http-request';
import PopupTemplate from '../common/PopupTemplate';
import ProductEdit from './ProductEdit';
import ring from '../../../public/images/ring.svg';
import auth from '../../auth';
import axios from 'axios';

class ProductDetailPage extends React.Component {
    render() {
        if(!this.props.params.pid){
            return (
                <PopupTemplate>
                    <div className="popup-header">
                        <span className="title">محصول جدید</span>
                    </div>
                    <div className="popup-main-content">
                        <ProductEdit product="new"/>
                    </div>
                </PopupTemplate>
            )
        }
        else{
            return (

                <Request
                    url={'https://freevery.com/api/web/product/get-by-id?id='+this.props.params.pid}
                    method='get'
                    accept='application/json'
                    verbose={true}
                >
                    {
                        ({error, result, loading}) => {
                            if (loading) {
                                return <div><ProductDetailPageContent loading={true} /></div>;
                            } else {
                                return (
                                    <div>
                                        <ProductDetailPageContent product={result.body.data[0]}/>
                                    </div>
                                );
                            }
                        }
                    }
                </Request>
            );
        }
    }
}

class ProductDetailPageContent extends React.Component {
    constructor(props,context){
        super(props);
    }
    render() {
        if(this.props.loading)
            return(
                <PopupTemplate>
                    <div className="popup-header">
                        <span className="title"> در حال بارگذاری</span>
                    </div>
                    <div className="popup-main-content">
                        <div style={{textAlign: 'center'}}>
                            <object data={ring} type="image/svg+xml">
                                <img src={ring}/>
                            </object>
                        </div>
                    </div>
                </PopupTemplate>
            )

        let user = null;
        let token = null;
        if(auth.loggedIn()){
            user = auth.getUser();
            token = auth.getToken();
        }
        if(user && user.role == '1')

            return (
                <PopupTemplate>
                    <div className="popup-header">
                        <span className="title">{this.props.product.title}</span>
                    </div>
                    <div className="popup-main-content">
                        <ProductEdit product={this.props.product}/>
                    </div>
                </PopupTemplate>
            );

        let imageName = this.props.product.image_name?this.props.product.image_name:this.props.product.code+'.jpg';
        return (
            <PopupTemplate>
                <div className="popup-header">
                    <span className="title">{this.props.product.title}</span>
                </div>
                <div className="popup-main-content">
                    <div className="col-md-12 product-image-in-popup">
                        <img style={{height:'100%'}} src={'/images/products/full/' + imageName}/>
                    </div>
                    <div className="col-md-12">
                        <div style={{textAlign:'center',fontSize:25}}> {this.props.product.price} تومان</div>
                    </div>
                    <div>
                        <p>
                        <strong>بارکد : </strong>{this.props.product.code}
                        </p>
                        <p>
                        <strong>پروانه ثبت : </strong>{this.props.product.parvane_sabt}
                        </p>
                        <p>
                        <strong>توضیحات : </strong>{this.props.product.description}
                        </p>
                    </div>
                    <div className="clearfix"></div>
                </div>
            </PopupTemplate>
        )
    }
}


export default ProductDetailPage;