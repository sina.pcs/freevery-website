import React from 'react';
import PopupTemplate from '../common/PopupTemplate';
import Login from '../common/Login';

class LoginPage extends React.Component {
    render() {
        return(
            <PopupTemplate>
                <div className="popup-header">
                    <span className="title"><span className="glyphicon glyphicon-user"/> ورود به سایت</span>
                </div>
                <div className="popup-main-content">
                    <Login />
                </div>
            </PopupTemplate>
        )
            ;
    }
};

export default LoginPage;