import React from 'react';
import Header from './common/Header/Header';
import Footer from './common/Footer';
import CategoryPage from './main/categories/CategoryPage';
import OrdersList from './common/OrdersList';

class App extends React.Component {
    constructor(props,context){
    super(props);
    this.state = {
        content:undefined
    };
}
    componentDidMount(){
        if(this.props.content != undefined)
        {
            this.setState({
                content:this.props.content
            });
        }
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.location.pathname != this.props.location.pathname) {
            if(nextProps.content != undefined)
            {
                this.setState({
                    content:nextProps.content
                });
            }
        }
    }
    render() {
        return (
            <div id="layoutContainer" className="container-fluid">
                <Header />
                <Popup popup={this.props.popup} />
                {this.props.content != false &&
                <main className="row">
                    <div className="col-md-9 col-sm-12 main-container">
                        <div className="Content">
                            {this.state.content || <CategoryPage />}
                        </div>
                        <Footer />
                    </div>
                    <div id="left-col" className="col-md-3 hide-sm">
                        <OrdersList />
                    </div>
                </main>
                }
            </div>
        );
    }
}

class Popup extends React.Component{
    render(){
        if(this.props.popup) {
            return (
                <div className="Popup">
                    {this.props.popup}
                </div>
            );
        }
        else return <div></div>
    }
}

export default App;
