import React from 'react';
import {Link} from 'react-router';
import MyInput from './input';
import categoriesData from '../../data/navigationMenu.json';



class CategoriesController extends React.Component{
    constructor(props,context){
        super(props);
        this.state = {
            openMenu: false,
            showSubMenu: -1
        };
        this.toggleMenu = this.toggleMenu.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
    }
    toggleMenu(){
        this.setState({
            openMenu: !this.state.openMenu
        })
    }
    showSubMenu(id){
        this.setState({
            showSubMenu: id
        })
    }
    closeMenu(e) {
        let c = document.getElementById('popupBack');
        let c2 = document.getElementById('closeButton');
        let c3 = document.getElementById('closeButton2');
        if(e.target == c || e.target == c2 || e.target == c3){
            this.setState({
                openMenu:false
            })
        }
    };
    render() {

        let navigation = categoriesData.map((category,i)=>{

            let subNavs = category.subCategories.map((subCategory)=>{

                let links = subCategory.hasChild?subCategory.subCategories.map((link) => {
                    let url = '/category/'+link.id+'/'+link.title;
                    return (
                        <li key={'category'+link.id}>
                            <Link onClick={()=>{this.setState({openMenu:false}); this.showSubMenu(-1)}} to={url}>{link.title}</Link>
                        </li>
                    );
                }): '';

                let url = 'category/'+subCategory.id+'/'+subCategory.title;
                let subCategoryHeader = <h3><Link to={url} onClick={()=>{this.setState({openMenu:false}); this.showSubMenu(-1)}}>{subCategory.title}</Link></h3>;
                return (
                    <div className="nav-column" key={'category'+subCategory.id}>
                        {subCategoryHeader}
                        <ul>
                            {links}
                        </ul>
                    </div>
                )
            });
            return (<li key={'category'+category.id}
                        onMouseEnter={()=>this.showSubMenu(category.id)}
                        onMouseLeave={()=>this.showSubMenu(-1)}
            >
                        <a href="#" onClick={(e)=>{e.preventDefault(); return false;}}>{category.title}</a>
                        <div className={this.state.showSubMenu===category.id?'open':''}>
                            {subNavs}
                        </div>
                    </li>)
        });


        return (
            <div>
                {this.state.openMenu && <div className="popup-back hidden-md hidden-lg hidden-desktop" id="popupBack" onClick={this.closeMenu}>
                    <div className="popup-content">
                        <button id="closeButton" className="btn btn-default popup-close-btn">
                            <span style={{pointerEvents:'none'}} className="glyphicon glyphicon-remove"/>
                        </button>

                        <div className="popup-header">
                            <span className="title">دسته بندی محصولات</span>
                        </div>
                        <div className="popup-main-content" style={{padding:0}}>
                            <div className="nav">
                                {navigation}
                            </div>
                        </div>
                    </div>
                </div>}

                <div className="nav-container">
                    <a className={'toggle-menu ' + (this.state.openMenu && 'active')} onClick={this.toggleMenu}>
                        <span className="glyphicon glyphicon-menu-hamburger"/>
                        <span className="hidden-sm" style={{verticalAlign: 'middle', fontSize: 14, marginRight: 10}}>دسته بندی</span>
                    </a>

                    <ul className={`nav hidden-xs hidden-sm `}>
                        {navigation}
                    </ul>

                </div>
            </div>
        )
    }
}

export default CategoriesController;