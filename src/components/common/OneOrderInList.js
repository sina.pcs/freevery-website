import React, { Component } from 'react';
import deleteImg from '../../../public/images/red-cross.png';

class OneOrderInList extends Component{
    constructor(props) {
        super(props);
        this.state = {willBeDeleted: false};
        this.delete = this.delete.bind(this);
    }
    delete(e){
        e.preventDefault();
        if(!this.state.willBeDeleted){
            this.setState({willBeDeleted:true});
            setTimeout(()=>{
                this.setState({willBeDeleted:false});
                this.props.delete();
            },500);

        }
    }
    render(){
        let order = this.props.order;
        if(this.props.editable != undefined && this.props.editable == false) {
            return (
                <div className="order">
                    <div className="title">{order.title}</div>
                    <div className="qauntity">
                        {order.quantity}
                    </div>
                    <div className="price">{order.price}</div>
                </div>
            )
        }
        else {
            return (
                <div className={"order " + (this.state.willBeDeleted && 'willBeDeleted')}>
                    <div className="title">{order.title}</div>
                    <div className="qauntity">
                        <button onClick={this.props.increase} className="plus-button"/>
                        {order.quantity}
                        <button onClick={this.props.decrease} className="minus-button"/>
                    </div>
                    <div className="price">{order.price}</div>
                    <div className="delete">
                        <button onClick={this.delete}>
                            <img src={deleteImg}/>
                        </button>
                    </div>
                </div>
            )
        }
    }
}

export default OneOrderInList;
