import React, {Component} from 'react';
import {connect} from 'react-redux';
import OneOrderInList from './OneOrderInList';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router';
import * as orderActions from '../../actions/orderActions'

class OrdersList extends Component {
    constructor(props, context) {
        super(props);
        this.state = {
            orders: []
        };
        this.increase = this.increase.bind(this);
        this.decrease = this.decrease.bind(this);
        this.delete = this.delete.bind(this);
        this.clearOrderList = this.clearOrderList.bind(this);
    }

    increase(e, index, price) {
        e.preventDefault();
        this.props.actions.increaseOrder(index);
        this.props.actions.increaseTotal(parseFloat(price));
    }

    decrease(e, index, price) {
        e.preventDefault();
        if(this.props.orders[index].quantity > 1){
            this.props.actions.decreaseOrder(index);
            this.props.actions.decreaseTotal(parseFloat(price));
        }
    }

    delete(index, price, quantity) {
        this.props.actions.deleteOrder(index);
        this.props.actions.decreaseTotal(parseFloat(price) * quantity);
    }

    clearOrderList(){
        this.props.actions.clearOrder();
        this.props.actions.clearTotal();
    }

    render() {
        var rows = [];
        let totalNumberOfOrders = 0;

        function total() {
            return 0;
        }

        rows = this.props.orders.map(
            (order, index) => <OneOrderInList increase={(e)=> this.increase(e, index, order.price)}
                                              decrease={(e)=> this.decrease(e, index, order.price)}
                                              delete={()=> this.delete(index, order.price, order.quantity)}
                                              order={order} key={index}/>
        );

        this.props.orders.map(
            (order, index) => {
                totalNumberOfOrders += order.quantity;
            }
        );


        return (
            <div>
                <div id="orderList">
                    <div id="orders" className="swiper-container">
                        <h4 className="orderListTitle"><span className="glyphicon glyphicon-shopping-cart"/> سبد خرید
                        </h4>
                        <button style={{background: '#ff6060',width: '100%',padding: 10,color: '#fff'}}
                                onClick={this.clearOrderList}>
                            <span className="glyphicon glyphicon glyphicon-trash"/> خالی کردن سبد خرید</button>
                        <div className="order">
                            <div className="title">نام محصول</div>
                            <div className="qauntity">تعداد</div>
                            <div className="price">قیمت واحد</div>
                            <div className="delete">حذف</div>
                        </div>
                        <div className="orders-container">
                            {rows}
                        </div>
                    </div>
                    {this.props.orders.length > 0 &&
                    <div id="pay-box">
                        <a id="pay-button" href="#/checkout" >
                            <span style={{float: 'right'}}> تایید سفارش</span>
                            <span style={{float: 'left'}}>{this.props.total} تومان</span>
                        </a>
                    </div>
                }
                </div>

                <div className="mobile-cart">
                    <Link to={`/orders`} className="btn btn-success btn-lg"><span className="glyphicon glyphicon-shopping-cart"/></Link>
                    <div className="numberOfOrders">
                        {totalNumberOfOrders}
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        orders: state.orders,
        total: state.total
    }
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(orderActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrdersList)
