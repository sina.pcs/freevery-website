import React from 'react';
import PopupTemplate from '../common/PopupTemplate';
import MyInput from '../common/input';
import auth from '../../auth';
import ReCAPTCHA from 'react-google-recaptcha';
import axios from 'axios';


let captcha;
let forgotCaptcha;
class Login extends React.Component {
    constructor(props,context){
        super(props);
        this.state = {
            error: '',
            pageState: 0,
            username:'',
            password:''
        };
        Login.contextTypes = {
            router: React.PropTypes.object,
            location: React.PropTypes.object
        };
        this.onSubmit=this.onSubmit.bind(this);
        this.forgotPassword=this.forgotPassword.bind(this);
        this.recaptchaChanged = this.recaptchaChanged.bind(this);
        this.recaptchaChanged2 = this.recaptchaChanged2.bind(this);
    }
    componentDidMount(){
    }
    onSubmit(event) {
        event.preventDefault();
        let username = event.target[0].value;
        let password = event.target[1].value;
        this.setState({
            username:username,
            password:password
        });
        captcha.execute();  
    }
    recaptchaChanged(value) {
        if(this.state.username != '' && this.state.password != '') {
            this.setState({pageState: 1,error:''});
        }
        auth.login(this.state.username, this.state.password,value, (loggedIn) => {
            if (!loggedIn)
                return this.setState({error: 'مشخصات وارد شده اشتباه است', pageState: 0});

            window.location.reload()
            // const {location} = this.props;
            //
            // if (location && location.state && location.state.nextPathname) {
            //     this.props.router.replace(location.state.nextPathname)
            // } else {
            //     this.props.router.replace('/');
            // }
        });
    }
    forgotPassword(event) {
        event.preventDefault();
        let username = event.target[0].value;
        this.setState({
            username:username,
        });
        forgotCaptcha.execute();
    }
    recaptchaChanged2(value) {
        if(this.state.username != '') {
            this.setState({pageState: 1,error:''});
        }
        else return;
        let objectToSend = {
            username: this.state.username,
            recaptcha: value
        }
        let _this = this;
        axios.post('https://freevery.com/api/web/user/forget-password', objectToSend)
            .then(function (response) {
                if (response.data.code == '1') {
                    _this.setState({pageState: 3})
                }
                else{
                    _this.setState({pageState: 2,error:response.data.message});

                }
            })
            .catch(function (error) {
                _this.setState({pageState: 2});
            });
    }
    render() {
        switch (this.state.pageState){
            case 0:
                return(
                    <div key={'login-form'}>

                        <ValidationError error={this.state.error}/>
                        <form onSubmit={this.onSubmit} className="login-form">
                            <MyInput refs="mobile" name="mobile" label="شماره تلفن همراه" type="text" />
                            <MyInput refs="pass" name="password" label="کلمه عبور" type="password" />
                            <ReCAPTCHA
                                ref={(el) => { captcha = el; }}
                                sitekey="6LeLYCIUAAAAACcjStjB2OJk0aFkne43idyuOqyX"
                                size="invisible"
                                onChange={this.recaptchaChanged}
                            />
                            <button type="submit">
                                ورود
                            </button>

                            <button onClick={()=>{this.setState({pageState:2})}}>
                                کلمه عبور خود را فراموش کرده ام
                            </button>
                        </form>
                    </div>
                )
                break;

            case 1:
                return(
                    <div>
                        <div>لطفا منتظر بمانید</div>
                    </div>
                )
                break;

            case 2:
                return(
                    <div key={'forgot-form'}>
                        <ValidationError error={this.state.error}/>
                        <form onSubmit={this.forgotPassword} className="login-form">
                            <div style={{marginBottom:30}}>
                                شماره خود را وارد کنید. کلمه عبور موقتی برای شما ارسال خواهد شد.
                            </div>
                            <MyInput refs="mobile" name="mobile" label="شماره تلفن همراه" type="text" />

                            <ReCAPTCHA
                                ref={(el) => { forgotCaptcha = el; }}
                                sitekey="6LeLYCIUAAAAACcjStjB2OJk0aFkne43idyuOqyX"
                                size="invisible"
                                onChange={this.recaptchaChanged2}
                            />
                            <button type="submit">
                                تایید
                            </button>
                        </form>
                    </div>
                )
                break;

            case 3:
                return(
                    <div>
                        <div>پیامکی برای شما ارسال شد. لطفا حداقل 10 دقیقه منتظر بمانید و در صورت عدم دریافت پیامک بعد از 10 دقیقه مجددا اقدام کنید. </div>
                    </div>
                )
                break;
        }

    }
};


class ValidationError extends React.Component {
    render() {
        if (this.props.error != '') {
            return (
                <div className="alert alert-danger" style={{marginBottom: 50,marginBottom: 40}}>
                    {this.props.error}
                </div>
            )
        }
        else
            return <div></div>
    }
}


export default Login;