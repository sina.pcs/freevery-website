import React from 'react';
import {Link, IndexLink} from 'react-router';
const Footer = () => {
    return (
        <footer>
            <div className="row footer-title" >
                <div className="col-md-4">
                    <span className="glyphicon glyphicon-earphone"/> 021-44148536
                </div>
                <div className="col-md-4">
                    <span className="glyphicon glyphicon-envelope"/> info@freevery.com
                </div>
                <div className="col-md-4">
                    <IndexLink className="logo" to="/"><img src="/images/freevery.png"/></IndexLink>
                </div>
            </div>
            <div className="row footer-content">
            <div className="col-md-4 col-sm-6">

                <Link className="btn btn-default" activeClassName="active" to="/about">
                    <span className="glyphicon glyphicon-info-sign"/><span> درباره ما</span>
                </Link>

                <Link className="btn btn-default" activeClassName="active" to="/contact">
                    <span className="glyphicon glyphicon-earphone"/><span> تماس با ما</span>
                </Link>
                <Link className="btn btn-default" activeClassName="active" to="/rules">
                    <span className="glyphicon glyphicon-question-sign"/><span> قوانین و مقررات</span>
                </Link>
            </div>
            <div className="col-md-4 col-sm-6">

                <a href="" className="market-link ios" />
                <a href="" className="market-link android" />

            </div>
            <div className="col-md-4 col-sm-6">

                <a className="samandehi" onClick={() => {
                    window.open("https://logo.samandehi.ir/Verify.aspx?id=83394&p=mcsixlaoxlaopfvlaods", "Popup", "toolbar=no, scrollbars=no, location=no, statusbar=no, menubar=no, resizable=0, width=450, height=630, top=30")
                }}>
                    <img id='oeukrgvjrgvjsizpwlao' style={{cursor: 'pointer'}}
                         alt='logo-samandehi'
                         src='https://logo.samandehi.ir/logo.aspx?id=83394&p=aqgwqftiqftibsiyshwl'/>
                </a>

                <a className="enamad" onClick={() => {
                    window.open("https://trustseal.enamad.ir/Verify.aspx?id=66947&p=hwmbhwmbjzpgodshqgwl", "Popup", "toolbar=no, location=no, statusbar=no, menubar=no, scrollbars=1, resizable=0, width=580, height=600, top=30")
                }}>
                    <img id='brgwbrgwgwmdhwlalbrh' style={{cursor: 'pointer'}}
                         alt='' src='https://trustseal.enamad.ir/logo.aspx?id=66947&p=kzoekzoezpfvaodspeuk'/>
                </a>
                <div style={{clear: 'both'}}/>
            </div>
            </div>
        </footer>
    )
}

export default Footer;