import React from 'react';
import Request from 'react-http-request';
import PopupTemplate from '../common/PopupTemplate';
import MyInput from '../common/input';
import ReCAPTCHA from 'react-google-recaptcha';
import ring from '../../../public/images/ring.svg';
import auth from '../../auth';
import axios from 'axios';


let captcha;
let captcha2;
let captcha3;
class SignUp extends React.Component {

    constructor(props, context) {
        super(props);
        this.state = {
            validationError: '',
            signUpState: 0, //show form state
            mobile: '',
            password: '',
            smsCode: '',
            captcha2: '',
            minToActivateButton:'01',
            secToActivateButton:'00',
            reSendButton:false,
            recaptchaValue: ''
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.checkCode = this.checkCode.bind(this);
        this.initializeClock = this.initializeClock.bind(this);
        this.sendSms = this.sendSms.bind(this);
        this.recaptchaChanged = this.recaptchaChanged.bind(this);
        this.signup = this.signup.bind(this);
    }

    componentWillUnmount(){
        clearInterval(this.timeinterval);
    }
    
    getTimeRemaining(deadline) {
        var t = Date.parse(deadline) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        return {
            'total': t,
            'minutes': minutes,
            'seconds': seconds
        };
    }

    static timeinterval = null;
    initializeClock() {
        clearInterval(this.timeinterval);
        let deadline = new Date(Date.parse(new Date()) +  1 * 60 * 1000);
        let t = this;
        this.timeinterval = setInterval((t) => {
            var t = this.getTimeRemaining(deadline);

            this.setState({minToActivateButton:('0' + t.minutes).slice(-2)});
            this.setState({secToActivateButton:('0' + t.seconds).slice(-2)});


            if (t.total <= 0) {
                this.setState({reSendButton:true});
                clearInterval(this.timeinterval);
                this.setState({minToActivateButton:'0'});
                this.setState({secToActivateButton:'0'});
            }
        }, 1000);
    }

    Validation = function (mobile, pass1, pass2) {

        let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let mobileRegex = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;

        let re = /[A-Za-z\d]{8,}/;

        if (!mobileRegex.test(mobile)) {
            this.setState({
                validationError: 'شماره وارد شده معتبر نیست',
                signUpState: 0,
                mobile: mobile
            });
            return false;
        }

        if (!re.test(pass1)) {
            this.setState({
                validationError: 'کلمه عبور حداقل باید 8 کاراکتر داشته باشد',
                signUpState: 0,
                mobile: mobile
            });
            return false;
        }

        if (pass1 != pass2) {
            this.setState({
                validationError: 'کلمه عبور و تکرار کلمه عبور یکسان نیستند',
                signUpState: 0,
                mobile: mobile,
            });
            return false
        }

        this.setState({
            validationError:''
        });
        return true;
    };

    onSubmit(event) {
        event.preventDefault();
        let mobile = this.refs.mobile.refs.text.value;
        let password = this.refs.password.refs.text.value;
        let passwordConfirm = this.refs.passwordConfirm.refs.text.value;

        if (this.Validation(mobile, password, passwordConfirm)) {
            this.setState({
                mobile: mobile,
                password: password,
            })
                captcha.execute();
        }

    }
    
    sendSms(captchaVal){
        this.setState({
                signUpState:3
            })

        axios.post('https://freevery.com/api/web/sms/send-sms', {
            headers: {
                'Content-Type': 'application/json'
            },
            mobile: this.state.mobile,
            recaptcha: captchaVal
        })
            .then((response) => {
                if (response.data.code == '1') {
                    this.setState({signUpState: 1,reSendButton: false});
                    this.initializeClock();
                }
                else{
                    this.setState({
                        signUpState: 0,
                        validationError: response.data.message
                    })                    
                }
            })
            .catch(function (error) {
                    this.setState({
                        signUpState: 0,
                        validationError: 'متاسفانه خطایی رخ داد. لطفا مجددا تلاش کنید'
                    })
            });

    }

    checkCode(e) {
        e.preventDefault();
        let smsCode = this.refs.smsCode.refs.text.value;
        captcha2.execute();
        
        this.setState({
            smsCode: smsCode
        });

    }

    recaptchaChanged(captchaVal) {

        this.setState({  
            captcha2:captchaVal,
            signUpState: 3
        })

        axios.post('https://freevery.com/api/web/sms/validate-code', {
            headers: {
                'Content-Type': 'application/json'
            },
            mobile: this.state.mobile,
            recaptcha: captchaVal,
            code: this.state.smsCode
        })
            .then((response) => {
                if (response.data.code == '1') {
                    this.signup();
                }
                else{
                    this.setState({
                        signUpState: 1,
                        validationError: 'کد وارد شده اشتباه است'
                    })
                }
            })
            .catch(function (error) {
                    this.setState({
                        signUpState: 1,
                        validationError: 'متاسفانه خطایی رخ داد. لطفا مجددا تلاش کنید'
                    })
            });

    }

    signup(){
        axios.post('https://freevery.com/api/web/user/sign-up', {
            headers: {
                'Content-Type': 'application/json'
            },
            username: this.state.mobile,
            password: this.state.password
        })
            .then((response) => {
                if (response.data.code == '1') {
                    localStorage.user = JSON.stringify(response.data.data);
                    localStorage.userToken = response.data.token;
                    window.location.reload()
                }
                else{
                    this.setState({
                        signUpState: 1,
                        validationError: 'متاسفانه خطایی رخ داد. لطفا مجددا تلاش کنید'
                    })
                }
            })
            .catch(function (error) {
                    this.setState({
                        signUpState: 1,
                        validationError: 'متاسفانه خطایی رخ داد. لطفا مجددا تلاش کنید'
                    })
            });
    }

    render() {
        switch (this.state.signUpState) {
            case 0 :
                return (
                    <div>
                        <ValidationError error={this.state.validationError}/>
                        <form onSubmit={this.onSubmit} key={1}>
                            <MyInput name="mobile" ref="mobile" label="شماره تلفن همراه" type="text"/>

                            <MyInput name="password" ref="password" label="کلمه عبور (حداقل 8 کاراکتر)"
                                     type="password"/>
                            <MyInput name="passwordConfirm" ref="passwordConfirm" label="تکرار کلمه عبور"
                                     type="password"/>

                                <ReCAPTCHA
                                    ref={(el) => { captcha = el; }}
                                    sitekey="6LeLYCIUAAAAACcjStjB2OJk0aFkne43idyuOqyX"
                                    size="invisible"
                                    onChange={this.sendSms}
                                />

                            <button type="submit">
                                ثبت نام
                            </button>
                        </form>
                    </div>
                );
            case 1 :
                return (
                    <div>
                        <ValidationError error={this.state.validationError}/>
                        <form onSubmit={this.checkCode} key={2}>
                            <div style={{marginBottom:40}}>کد ارسال شده از طریق پیامک به شماره {this.state.mobile} را وارد کنید</div>
                            <MyInput name="smsCode" ref="smsCode" label="کد" type="text"/>

                                <ReCAPTCHA
                                    ref={(el) => { captcha2 = el; }}
                                    sitekey="6LeLYCIUAAAAACcjStjB2OJk0aFkne43idyuOqyX"
                                    size="invisible"
                                    onChange={this.recaptchaChanged}
                                />

                            <button type="submit">
                                تایید
                            </button>

                            </form>

                            <div style={{textAlign: 'center', padding:20}}>
                                دکمه ارسال مجدد بعد از 1 دقیقه برای شما فعال خواهد شد. در صورت عدم دریافت پیامک پس از 1 دقیقه بر روی این دکمه کلیک کنید.
                            </div>

                            {!this.state.reSendButton &&
                            <div style={{textAlign: 'center', padding:20,fontSize:30}}>
                                {this.state.minToActivateButton}:{this.state.secToActivateButton}
                            </div>
                            }
                            {this.state.reSendButton &&
                                <form onSubmit={(e)=>{e.preventDefault(); captcha3.execute();}}>
                                    <ReCAPTCHA
                                        ref={(el) => { captcha3 = el; }}
                                        sitekey="6LeLYCIUAAAAACcjStjB2OJk0aFkne43idyuOqyX"
                                        size="invisible"
                                        onChange={this.sendSms}
                                    />
                                    <button type="submit" className="transparent-btn">
                                        ارسال مجدد پیامک
                                    </button>
                                </form>
                            }
                    </div>
                );
            case 2 :
                return (
                        <div>
                            ثبت نام با موفقیت انجام شد و شما وارد سایت شدید.
                        </div>
                )

            case 3:
                return (
                    
                        <div style={{textAlign: 'center'}}>
                            <object data={ring} type="image/svg+xml">
                                <img src={ring}/>
                            </object>
                        </div>
                )
        }

    }
}

class ValidationError extends React.Component {
    render() {
        if (this.props.error != '') {
            return (
                <div className="alert alert-danger" style={{marginBottom: 50,marginBottom: 40}}>
                    {this.props.error}
                </div>
            )
        }
        else
            return <div></div>
    }
}


export default SignUp;