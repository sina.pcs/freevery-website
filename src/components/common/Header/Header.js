import React from 'react';
import {Link, IndexLink} from 'react-router';
import HeaderLoginButtons from './HeaderLoginButtons';
import NavigationMenu from '../NavigationMenu';

const Header = () => {
    return (
        <div id="header">
            <header className="row">
                <div className="col-md-6 col-sm-6">
                    <IndexLink className="logo" to="/">
                        <span className="hidden-xs">سوپرمارکت اینترنتی</span>
                        <img src="/images/freevery.png"/>
                    </IndexLink>
                </div>
                <div className="col-md-3 col-sm-2">
                </div>
                <HeaderLoginButtons />
            </header>

            <NavigationMenu />
        </div>
    )
}

export default Header;