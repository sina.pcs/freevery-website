import React from 'react';
import auth from '../../../auth';
import {Link , IndexLink} from 'react-router';

let loggedIn = false;
class HeaderLoginButtons extends React.Component {

    showMenu(){
        document.getElementById('user-menu').className = 'show';
        document.getElementById('user-menu-btn').className = 'btn btn-default show';
    }
    hideMenu(){
        document.getElementById('user-menu').className = '';
        document.getElementById('user-menu-btn').className = 'btn btn-default';
    }
    render() {
        let loggedIn=auth.loggedIn();
        if(loggedIn){
            let user = auth.getUser();
            return (
                <div className="col-md-3 col-sm-4 header-buttons" onMouseLeave={this.hideMenu} >
                    <a className="btn btn-default" id="user-menu-btn" onMouseMove={this.showMenu} style={{width:'100%',padding:'0 10px',position:'relative',zIndex:3}}>

                        <span id="user-name">
                            <span className="glyphicon glyphicon-menu-hamburger"/> {user.first_name?user.first_name:user.username}
                        </span>
                        <span id="user-balance">
                            <span className="glyphicon glyphicon-briefcase"/> {user.balance?user.balance:0} تومان
                        </span>
                    </a>
                    <div id="user-menu">
                        {user.role == '1' && <Link className="btn btn-default" to="/product/new" >محصول جدید</Link>}
                        <Link className="btn btn-default" to="/profile" >تنظیمات پروفایل</Link>
                        <Link className="btn btn-default" to="/history" >سابقه سفارشات</Link>
                        <Link className="btn btn-default" to="/withdraw" >افزایش موجودی</Link>
                        <Link className="btn btn-default" to="/logout" >خروج</Link>
                    </div>
                </div>
            )
        }
        else {
            return (
                <div className="col-md-3 col-sm-4 header-buttons">
                    <Link className="btn btn-default" to="/login">
                        <span className="glyphicon glyphicon-user"/> ورود
                    </Link>
                    <Link className="btn btn-default" activeClassName="active" to="/signup">
                        <span className="glyphicon glyphicon-edit"/> عضویت
                    </Link>
                </div>
            )
        }
    }
}


export default HeaderLoginButtons;
