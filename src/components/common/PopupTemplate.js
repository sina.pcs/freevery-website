import React from 'react'

export default class PopupTemplate extends React.Component{
    closePopup = function (e) {
        let c = document.getElementById('popupBack');
        let c2 = document.getElementById('closeButton');
        let c3 = document.getElementById('closeButton2');
        if(e.target == c || e.target == c2 || e.target == c3){
            window.history.back();
        }
    };
    render() {
        return (
            <div className="popup-back" id="popupBack" onClick={this.closePopup}>
                <div className="popup-content">
                    <button id="closeButton" className="btn btn-default popup-close-btn">
                        <span style={{pointerEvents:'none'}} className="glyphicon glyphicon-remove"/>
                    </button>
                    {this.props.children}
                </div>
            </div>
        )
    }
}