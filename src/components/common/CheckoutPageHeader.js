import React from 'react';
import {Link} from 'react-router';
import MyInput from '../common/input';
import {ComboBox, Option} from 'belle';

class CheckoutPageHeader extends React.Component {
    render() {
        return(
            <div className="row checkout-header">
                <div className={'col-sm-6 '+(this.props.active==0 && 'active')} >
                    <span className="active-number">مرحله اول</span>
                    ورود / ثبت نام
                </div>
                <div className={'col-sm-6 '+(this.props.active==1 && 'active')}>
                    <span className="active-number">مرحله دوم</span>
                    ورود مشخصات ارسال
                </div>
            </div>
        )

    }
}


export default CheckoutPageHeader;