import React from 'react';

class MyInput extends React.Component{
    render() {
        return (
            <div className={"group "+this.props.className}>
                <input type={this.props.type}
                       name={this.props.name}
                       ref="text"
                       value={this.props.value}
                       onChange={this.props.onChange?this.props.onChange:()=>{return false}}
                       required />
                <span className="highlight"/>
                <span className="bar"/>
                <label>{this.props.label}</label>
            </div>
        );
    }
}

export default MyInput;