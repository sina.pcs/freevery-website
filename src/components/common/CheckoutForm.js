import React from 'react';
import {Link} from 'react-router';
import MyInput from '../common/input';
import {ComboBox, Option} from 'belle';
import auth from '../../auth';
import axios from 'axios';;
import ring from '../../../public/images/ring.svg';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as orderActions from '../../actions/orderActions';
import { Steps, Hints } from 'intro.js-react';
import 'intro.js/introjs.css';
import 'intro.js/introjs-rtl.css';



const areas = [
    {id: '1', name: 'ونک'},
    {id: '2', name: 'میرداماد'},
    {id: '3', name: 'جردن'},
];

class CheckoutForm extends React.Component {
    constructor(props,context){
        super(props);
        this.state = {
            first_name: '',
            mobile: '',
            email:'',
            last_name: '',
            address: [],
            pageState: 0,
            addressAddedBefore: false,
            selectedAddress:0,
            payment_method: '',
            requiredFieldsError : {id:'',msg:''},
            validationError:'',
            showPopup: false
        };
        this.handleSubmit=this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.appendAddress=this.appendAddress.bind(this);
        this.editAddress=this.editAddress.bind(this);
        this.areaChanged=this.areaChanged.bind(this);
        this.onAddressChanged = this.onAddressChanged.bind(this);
    }

    componentDidMount(){
        document.title = "Freevery - ورود مشخصات ارسال";
        if(this.props.total < 15000 && this.props.orders.findIndex(product=>product.pid == 1) === -1)
            this.setState({showPopup:true})
    }
    componentWillMount(){
        if(auth.loggedIn()){
            let user = auth.getUser();
            this.setState({
                first_name:user.first_name?user.first_name:'',
                last_name:user.last_name?user.last_name:'',
                mobile:user.mobile?user.mobile:'',
                email:user.email?user.email:'',
                address:user.address?user.address:[],
            });
        }

    }

    componentWillReceiveProps(np){
        if(np.total < 15000 && np.orders.findIndex(product=>product.pid == 1) === -1)
            this.setState({showPopup:true})
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? (target.checked?1:0) : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }
    onAddressChanged(e){
        this.setState({
            selectedAddress: parseInt(e.currentTarget.value)
        });
    }
    appendAddress(e) {
        let newInput = {"address":"","area_id":undefined,"areaName":''};
        this.setState({
            address: this.state.address.concat([newInput]),
            addressAddedBefore: true,
            selectedAddress: this.state.address.length
        });
    }
    areaChanged(i,event){
        let address = this.state.address;
        address[i].area_id = event.identifier;
        address[i].areaName = event.value;
        this.setState({ address: address });
    }
    editAddress(i,event) {
        let address = this.state.address;
        address[i].address = event.target.value;
        this.setState({ address: address });
    }

    handleSubmit(e){
        e.preventDefault();
        if(this.state.first_name == '' || this.state.last_name == '' || this.state.email=='')
        {
            this.setState({
                requiredFieldsError : {id:'name-fields',msg:'پر کردن تمامی موارد الزامی است'}
            });
            document.querySelector('#name-fields').scrollIntoView({
                behavior: 'smooth'
            });

            return;
        }
        if(this.state.payment_method==''){
            this.setState({
                requiredFieldsError : {id:'pay-field',msg:'لطفا نوع پرداخت را مشخص کنید'}
            });
            document.querySelector('#pay-field').scrollIntoView({
                behavior: 'smooth'
            });

            return;
        }
        if(this.state.address.length == 0)
        {
            this.setState({
                requiredFieldsError : {id:'address-field',msg:'با کلیک بر روی دکمه اضافه کردن آدرس، آدرس خود را اضافه کنید.'}
            });
            document.querySelector('#address-field').scrollIntoView({
                behavior: 'smooth'
            });
            return;
        }
        let address = this.state.address[this.state.selectedAddress];
        if(address.address == '' || address.area_id == undefined )
        {
            this.setState({
                requiredFieldsError : {id:'area-field',msg:'منطقه و آدرس را به درستی وارد نکرده اید'}
            });
            document.querySelector('#area-field').scrollIntoView({
                behavior: 'smooth'
            });
            return;
        }


        this.setState({
            pageState: 1
        });

        let _this = this;
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + auth.getToken() ;

        let user = auth.getUser();
        let orders = this.props.orders.map((order) => {
            return {
                "product_id":order.pid,
                "price":order.price,
                "count":order.quantity,
                "product_title":order.title,
                "product_category_id":order.categoryId}
        });
        let objectToSend = {
            "customer_fullname": this.state.first_name + ' ' + this.state.last_name,
            "customer_mobile": user.username,
            "customer_address":address.address,
            "area_id":address.area_id,
            "area":  areas.find((a) => {
                return a.id == address.area_id
            }).name,
            "payment_method":this.state.payment_method,
            "addresses":this.state.address,
            "orders":orders
        }
        axios.post('https://freevery.com/api/web/orders/create', objectToSend)
            .then(function (response) {
                if (response.data.code == '1') {
                    user.address = _this.state.address;
                    auth.updateUser(user);
                    _this.props.actions.clearOrder();
                    _this.props.actions.clearTotal();
                    _this.props.changeState(response.data.data.id);
                }
                else{
                    _this.setState({pageState: 0,validationError:response.data.message});

                }
            })
            .catch(function (error) {
                _this.setState({pageState: 0});
            });
    }

    addDeliveryProduct = () => {
        this.props.actions.addOrder({
            categoryId:"1",
            pid:"1",
            price:"2000",
            quantity:1,
            title:"هزینه ارسال"
        });
        this.props.actions.increaseTotal(2000);
        this.setState({showPopup:false})
    }

    render() {
        switch (this.state.pageState) {
            case 0 :
                return (
                    <div className=" panel panel-default" style={{marginTop: 20}}>
                        {this.state.showPopup && <div className="popup-back" id="popupBack">
                            <div className="popup-content" style={{height: 'auto'}}>
                                <div className="popup-header">
                                    <span className="title">هزینه ارسال</span>
                                </div>
                                <div className="popup-main-content">
                                    با توجه به اینکه مقدار سفارش شما از 15000 تومان کمتر است 2000 تومان هزینه ارسال به سبد خرید
                                    شما اضافه خواهد شد.
                                    <div className="delivery-popup-buttons">
                                        <button onClick={this.addDeliveryProduct} className="btn btn-success">تایید و ادامه سفارش</button>
                                        <Link to={`/`} className="btn btn-warning">افزایش محصول به سبد خرید</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                        }
                        <div className="panel-heading">
                            <h3 className="panel-title">اطلاعات ارسال</h3>
                        </div>
                        <div  id="checkout-form" className="panel-body">
                            {this.state.requiredFieldsError.id == 'name-fields' &&
                            <div id={this.state.requiredFieldsError.id} className="alert alert-danger" style={{marginBottom: 10}}>
                                {this.state.requiredFieldsError.msg}
                            </div>
                            }
                            <ValidationError error={this.state.validationError}/>
                            <div className="alert alert-info" style={{marginBottom: 50}}>
                                از طریق صفحه <Link to="/profile" style={{color:'rgb(22, 139, 255)'}}>تنظیمات پروفایل</Link> می توانید اطلاعات کاربری خود را ذخیره کنید.
                            </div>
                            <form onSubmit={this.handleSubmit}>
                                <MyInput className="col col-sm-6"
                                         name="first_name"
                                         label="نام"
                                         type="text"
                                         value={this.state.first_name}
                                         onChange={this.handleInputChange}
                                />
                                <MyInput className="col col-sm-6"
                                         name="last_name"
                                         label="نام خانوادگی"
                                         type="text"
                                         value={this.state.last_name}
                                         onChange={this.handleInputChange}
                                />

                                <MyInput className="col col-sm-12"
                                         name="email"
                                         label="ایمیل"
                                         type="text"
                                         value={this.state.email}
                                         onChange={this.handleInputChange}
                                />

                                <div className="col col-sm-12 alert form-group address-field">
                                    {this.state.requiredFieldsError.id == 'address-field' &&
                                    <div id={this.state.requiredFieldsError.id} className="alert alert-danger" style={{marginBottom: 10}}>
                                        {this.state.requiredFieldsError.msg}
                                    </div>
                                    }
                                    {this.state.requiredFieldsError.id == 'area-field' &&
                                    <div id={this.state.requiredFieldsError.id} className="alert alert-danger" style={{marginBottom: 10}}>
                                        {this.state.requiredFieldsError.msg}
                                    </div>
                                    }
                                    <div className="row">
                                        <div className="col-sm-12"
                                             style={{fontSize: 14, color: '#999', marginBottom: 20}}>
                                            انتخاب آدرس
                                        </div>
                                        {this.state.address.map((input, i) => {
                                                return (
                                                    <div key={i} className="col col-sm-12">
                                                        <label className="col col-sm-12 radio-inline">
                                                            <input type="radio"
                                                                   name="selectedAddress"
                                                                   value={i}
                                                                   checked={this.state.selectedAddress === i}
                                                                   onChange={this.onAddressChanged}
                                                                   style={{marginTop: 30}}/>
                                                            <div className="group col col-sm-4 combo-box">
                                                                <ComboBox id={'area' + (i)}
                                                                          defaultValue={input.area_id ? areas.find((a) => {
                                                                              return a.id == input.area_id
                                                                          }).name : input.areaName}
                                                                          placeholder="انتخاب منطقه"
                                                                          onUpdate={(event) => {
                                                                              this.areaChanged(i, event);
                                                                          }}>
                                                                    {areas.map((area, index) => {
                                                                        return (
                                                                            <Option value={ area.name }
                                                                                    identifier={ area.id }
                                                                                    key={ 'area' + i + index }>
                                                                                { area.name }
                                                                            </Option>
                                                                        );
                                                                    })
                                                                    }
                                                                </ComboBox>
                                                            </div>
                                                            <MyInput
                                                                id={'address' + (i)}
                                                                className="col col-sm-8"
                                                                name="address"
                                                                value={input.address}
                                                                onChange={(e) => {
                                                                    this.editAddress(i, e)
                                                                }}
                                                                key={'address' + i}
                                                                label="آدرس" type="text"/>
                                                        </label>

                                                    </div>
                                                )
                                            }
                                        )}
                                    </div>

                                    {
                                        !this.state.addressAddedBefore &&
                                        <button className="transparent-btn"
                                                onClick={ (e) => this.appendAddress(e) }
                                                style={{width: 200, marginTop: 10}}>
                                            اضافه کردن آدرس
                                        </button>
                                    }
                                </div>
                                <div className="pay-field alert form-group col-sm-12">
                                    {this.state.requiredFieldsError.id == 'pay-field' &&
                                    <div id={this.state.requiredFieldsError.id} className="alert alert-danger" style={{marginBottom: 10}}>
                                        {this.state.requiredFieldsError.msg}
                                    </div>
                                    }
                                    <div className="row">
                                        <div className="col-sm-12"
                                             style={{fontSize: 14, color: '#999', marginBottom: 20}}>
                                            نوع پرداخت:
                                        </div>
                                        <div className="col-sm-12">
                                            <label className="radio-inline">
                                                <input type="radio"
                                                       name="payment_method"
                                                       value="1"
                                                       checked={this.state.payment_method === "1"}
                                                       onChange={(e) => this.setState({payment_method: "1"})}
                                                />
                                                پرداخت آنلاین
                                            </label>
                                            <label className="radio-inline">

                                                <input type="radio"
                                                       name="payment_method"
                                                       value="2"
                                                       checked={this.state.payment_method === "2"}
                                                       onChange={(e) => this.setState({payment_method: "2"})}
                                                />
                                                پرداخت نقدی درب منزل
                                            </label>
                                            <label className="radio-inline">
                                                <input type="radio"
                                                       name="payment_method"
                                                       value="3"
                                                       checked={this.state.payment_method === "3"}
                                                       onChange={(e) => this.setState({payment_method: "3"})}
                                                />
                                                پرداخت از طریق دستگاه POS درب منزل
                                            </label>
                                        </div>
                                    </div>
                                    <div className="alert alert-danger" style={{marginTop: 20}}>
                                        دقت فرمایید که تمامی تامین کنندگان ما دارای دستگاه POS نمی باشند و در صورت
                                        انتخاب روش پرداخت از طریق دستگاه POS ممکن است سفارش شما دیرتر از سایر روش ها
                                        تایید گردد.
                                    </div>

                                </div>

                                <button type="submit">
                                    ثبت سفارش
                                </button>
                            </form>
                        </div>
                    </div>
                )
                break;
            case 1:
                return (
                    <div style={{textAlign: 'center',marginTop:40}}>
                        <object data={ring} type="image/svg+xml">
                            <img src={ring}/>
                        </object>
                    </div>
                )
                break;
        }

    }
}
class ValidationError extends React.Component {
    render() {
        if (this.props.error != '') {
            return (
                <div className="alert alert-danger" style={{marginBottom: 50,marginBottom: 40}}>
                    {this.props.error}
                </div>
            )
        }
        else
            return <div></div>
    }
}

function mapStateToProps(state, ownProps) {
    return {
        orders: state.orders,
        total: state.total
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(orderActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutForm)