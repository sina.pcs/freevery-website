import React, {Component} from 'react';
import {connect} from 'react-redux';
import OneOrderInList from './OneOrderInList';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router';
import auth from '../../auth';
import axios from 'axios';
import * as orderActions from '../../actions/orderActions';
import ring from '../../../public/images/ring.svg';
import PopupTemplate from '../common/PopupTemplate';
import feathers from 'feathers-client';
import io from 'socket.io-client';

const jalaali = require('jalaali-js');


let orderService;
class OrderStatus extends Component {
    constructor(props, context) {
        super(props);
        this.state = {
            order: null,
            loading:true,
            orderCancelDialog:false

        }
        this.getOrderById = this.getOrderById.bind(this);
        this.webSocketConnect = this.webSocketConnect.bind(this);
        this.cancelOrder = this.cancelOrder.bind(this);
        this.resetOrder = this.resetOrder.bind(this);
        this.confirmOrder = this.confirmOrder.bind(this);
        this.goToPayment = this.goToPayment.bind(this);
        this.closePopup = this.closePopup.bind(this);
    }
    componentDidMount(){

        //const host = 'https://freevery.com/ws/';
        const socket = io.connect('https://freevery.com:3031',{secure:true});
        const app = feathers()
            .configure(feathers.socketio(socket))
            .configure(feathers.hooks());

        orderService = app.service('orders');

        this.getOrderById(this.props.params.orderId);
    }

    webSocketConnect(){
        orderService.on('patched',(order)=>{
            debugger;
            if(order.id == this.props.params.orderId){
                this.getOrderById(order.id);
            }
        })
    }
    getOrderById(orderId){
        let _this = this;
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + auth.getToken();
        axios.get('https://freevery.com/api/web/orders/get-by-orderid?order_id=' + orderId)
            .then(function (response) {
                if (response.data.code = "1") {
                    _this.setState(
                        {
                            order: response.data.data[0],
                            loading:false
                        }
                    )
                    if(response.data.data[0].state != 1){

                    }
                    if(response.data.data[0].state != 7)
                        _this.webSocketConnect();
                }
            })
            .catch(function (error) {
                if(error.response.data.status == "401")
                {
                    _this.props.router.replace('/logout');
                }
                else
                    _this.getOrderById(orderId);
            });
    }
    cancelOrder(){
        this.setState({
            loading:true,
            orderCancelDialog: false
        })
        let _this = this;
        let orderId = this.state.order.id;
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + auth.getToken();
        axios.put('https://freevery.com/api/web/orders/cancel-order-by-user?order_id=' + orderId)
            .then(function (response) {
                if (response.data.code = "1") {
                    _this.setState(
                        {
                            order: response.data.data[0],
                            loading:false
                        }
                    )
                }
            })
            .catch(function (error) {
                if(error.response.data.status == "401")
                {
                    _this.props.router.replace('/logout');
                }
                else
                    _this.cancelOrder();
            });
    }

    resetOrder(){
        this.setState({
            loading:true,
            orderCancelDialog: false
        })
        let _this = this;
        let orderId = this.state.order.id;
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + auth.getToken();
        axios.put('https://freevery.com/api/web/orders/reset-order-by-user?order_id=' + orderId)
            .then(function (response) {
                if (response.data.code = "1") {
                    _this.setState(
                        {
                            order: response.data.data[0],
                            loading:false,
                        }
                    )
                }
            })
            .catch(function (error) {
                if(error.response.data.status == "401")
                {
                    _this.props.router.replace('/logout');
                }
                else
                    _this.cancelOrder();
            });
    }

    confirmOrder(){
        this.setState({
            loading:true
        });
        let _this = this;
        let orderId = this.state.order.id;
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + auth.getToken();
        axios.put('https://freevery.com/api/web/orders/confirm-delivery-by-user?order_id=' + orderId)
            .then(function (response) {
                if (response.data.code = "1") {
                    _this.setState(
                        {
                            order: response.data.data[0],
                            loading:false
                        }
                    )
                }
            })
            .catch(function (error) {
                if(error.response.data.status == "401")
                {
                    _this.props.router.replace('/logout');
                }
                else
                    _this.confirmOrder();
            });
    }
    goToPayment(){
        window.location = 'https://freevery.com/api/payment/index.php?order_id='+this.props.params.orderId+'&user_agent=web';
    }

    componentWillUnmount(){
        orderService.removeListener('patched');
    }

    closePopup = function (e) {
        let c = document.getElementById('popupBack');
        let c2 = document.getElementById('closeButton');
        let c3 = document.getElementById('closeButton2');
        if(e.target == c || e.target == c2 || e.target == c3){
            this.setState({
                orderCancelDialog: false
            })
        }
    };


    render() {
        if(this.state.orderCancelDialog && !this.loading){
            return (
                <div className="popup-back" id="popupBack" onClick={this.closePopup}>
                    <div className="popup-content">
                        <button id="closeButton" className="btn btn-default popup-close-btn">
                            <span style={{pointerEvents:'none'}} className="glyphicon glyphicon-remove"/>
                        </button>
                        <div className="popup-header">
                            <span className="title"><span className="glyphicon glyphicon-user"/>عدم تایید / لغو </span>
                        </div>
                        <div className="popup-main-content">
                            <p>
                                در صورتی که می خواهید سفارش شما به صورت کامل لغو شود بر روی دکمه لغو کامل سفارش کلیک کنید.
                            </p>
                            <button className="primary-btn" onClick={this.cancelOrder}>لغو کامل سفارش</button>
                            <p style={{marginTop:20}}>
                                در صورتی که تغییرات صورت گرفته را نمی پذیرید بر روی این دکمه کلیک کنید تا برای بررسی مجدد ارسال شود.
                            </p>
                            <button className="primary-btn" onClick={this.resetOrder}>عدم تایید تغییرات </button>
                        </div>
                    </div>
                </div>
            )
        }
        if(this.state.loading)
        {
            return(
                <div style={{textAlign: 'center'}}>
                    <object data={ring} type="image/svg+xml">
                        <img src={ring}/>
                    </object>
                </div>
            )
        }
        else {
            let productsPrice = 0;
            let products = this.state.order.orderDetail.map((product, index) =>
                {
                    productsPrice += parseInt(product.price) * parseInt(product.count);
                    return (
                        <tr key={index}>
                            <td>
                                {product.product_title}
                            </td>
                            <td>
                                {product.count}
                            </td>
                            <td>
                                {product.price}
                            </td>
                        </tr>
                    )
                }
            );

            let changedProducts;
            let changedProductsPrice = 0;
            if(this.state.order.state == 0)
                changedProducts = '';
            else
                changedProducts = this.state.order.orderDetail.map((product, index) => {
                        if(product.state == 0)
                            changedProductsPrice += parseInt(product.price) * parseInt(product.count);
                        else
                            changedProductsPrice += parseInt(product.confirmed_price) * parseInt(product.confirmed_count);

                        let classNames = product.state == 1?'edited':'';
                        classNames += product.state == 2?' deleted':'';
                        return (
                            <tr key={'changed' + index} className={classNames}>
                                <td>
                                    {product.state == 0? product.product_title :product.confirmed_product_title}
                                </td>
                                <td>
                                    {product.state == 0? product.count :product.confirmed_count}
                                </td>
                                <td>
                                    {product.state == 0? product.price :product.confirmed_price}
                                </td>
                            </tr>

                        );
                    }
                );
            let orderStatus = '';
            let headerText = '';
            let button = '';
            let cancelButton = '';

            switch (this.state.order.state) {
                case '-1':
                    headerText = ``
                    orderStatus = 'لغو شده'
                    break;
                case '0':
                    headerText = `با تشکر از شما. سفارش ثبت شد و در انتظار تایید از طرف تامین کنندگان ما است. به زودی و پس از تایید اطلاع رسانی خواهد شد.`
                    orderStatus = 'در انتظار تایید سوپرمارکت'
                    break;
                case '1':
                    if(this.state.order.payment_method == "1"){
                        headerText = `سفارش شما تایید شد و پس از پرداخت آنلاین ارسال خواهد شد.`;
                        orderStatus = 'در انتظار پرداخت آنلاین';
                        button = <button className="primary-btn" onClick={this.goToPayment}>پرداخت آنلاین</button>;
                    }
                    else{
                        headerText = 'سفارش شما تایید شد و برای شما ارسال خواهد شد'
                        orderStatus = 'سفارش شما تایید شد';
                    }
                    break;
                case '2':

                    if(this.state.order.payment_method == "1"){
                        headerText = 'سفارش شما با اندکی تغییر توسط سوپر مارکت تایید شده است. در صورت تایید تغییرات روی دکمه تایید سفارش کلیک کنید.';
                        orderStatus = 'در انتظار تایید شما و پرداخت آنلاین';
                        button = <button className="primary-btn" onClick={this.goToPayment}>تایید تغییرات و پرداخت آنلاین</button>;
                    }
                    else{
                        headerText = 'سفارش شما با اندکی تغییر توسط سوپر مارکت تایید شده است. در صورت تایید تغییرات روی دکمه تایید سفارش کلیک کنید.';
                        orderStatus = 'در انتظار تایید شما';
                        button = <button className="primary-btn" onClick={this.confirmOrder}>تایید تغییرات</button>;
                    }
                    cancelButton = <button className="danger-btn" onClick={()=>{
                        this.setState({
                            orderCancelDialog: true
                        })
                    }}>عدم تایید تغییرات</button>;
                    break;
                case '3':
                    orderStatus = 'پرداخت ناموفق'
                    button = <button className="primary-btn" onClick={this.goToPayment}>پرداخت مجدد</button>;
                    break;
                case '4':
                    if(this.state.order.payment_method == "1"){
                        headerText = ` سفارش شما به زودی ارسال خواهد شد.`;
                        orderStatus = 'پرداخت موفق';
                    }
                    else{
                        headerText = ` سفارش شما به زودی ارسال خواهد شد.`;
                        orderStatus = 'تایید شده توسط شما';
                    }
                    break;
                case '5':
                    orderStatus = 'در راه'
                    button = <div style={{textAlign:'center',marginTop:20,border:'2px solid #ddd'}}>
                        <div style={{margin:10}}>
                            لطفا این کد را پس از دریافت سفارش به نماینده سوپرمارکت تحویل دهید
                        </div>
                        <h3 style={{background:'#ddd',padding:10,margin:0}}>{this.state.order.pincode}</h3>
                    </div>
                    break;
                case '6':
                    orderStatus = 'تحویل داده شد'
                    break;
                case '7':
                    orderStatus = 'بسته شد'
                    break;

            }

            let t = this.state.order.insert_datetime.split(/[- :]/);
            let j = jalaali.toJalaali(parseInt(t[0]), parseInt(t[1]), parseInt(t[2]));
            return (
                <div style={{padding: 20}}>
                    {headerText != '' && <div className="alert alert-info">
                        {headerText}
                    </div>}
                    <div>
                        <span>{j.jy + '/' + j.jm + '/' + j.jd}</span>
                        <span> ساعت {`${t[3]}:${t[4]}`}</span>
                    </div>

                    <div className="supplier-status">
                        <span className="glyphicon glyphicon-time"/>
                        {orderStatus}
                    </div>
                    <div className="products-table-container">
                        <h4 className="products-header">
                            سفارش اصلی
                        </h4>
                        <table className="products-table" style={{width: '100%'}}>
                            <thead>
                            <tr>
                                <th>
                                    نام محصول
                                </th>
                                <th>
                                    تعداد
                                </th>
                                <th>
                                    قیمت واحد
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {products}

                            <tr>
                                <td>قیمت کل</td>
                                <td></td>
                                <td>{productsPrice} تومان</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div className="products-table-container">
                        <h4 className="products-header">
                            سفارش پذیرفته شده
                        </h4>
                        <table className="products-table" style={{width: '100%'}}>
                            <thead>
                            <tr>
                                <th>
                                    نام محصول
                                </th>
                                <th>
                                    تعداد
                                </th>
                                <th>
                                    قیمت واحد
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {changedProducts}
                            <tr>
                                <td>قیمت کل</td>
                                <td></td>
                                <td>{changedProductsPrice} تومان</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div style={{clear: 'both'}}/>
                    <div style={{marginTop:20}}>
                        <div style={{width:20,height:20,backgroundColor:'#afdeaf',display:'inline-block'}}/>
                        <div style={{lineHeight:'20px',display:'inline-block',verticalAlign:'top',marginRight:10}}>رنگ سبز نشانه تغییر قیمت و یا محصول توسط سوپرمارکت است</div>
                    </div>
                    <div>
                        <div style={{width:20,height:20,backgroundColor:'#FF1744',display:'inline-block'}}/>
                        <div style={{lineHeight:'20px',display:'inline-block',verticalAlign:'top',marginRight:10}}>رنگ قرمز نشانه حذف محصول از لیست سفارش توسط سوپرمارکت است. (به دلیل عدم موجودی)</div>
                    </div>
                    {button}
                    {cancelButton}
                </div>
            )
        }
    }
}

export default OrderStatus;
