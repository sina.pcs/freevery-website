import {combineReducers} from 'redux';
import orders from './orderReducer';
import total from './totalReducer';

const rootReducer = combineReducers({
   orders,total
});

export default rootReducer;