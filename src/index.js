import React from 'react';
import ReactDOM from 'react-dom';
import {Router, hashHistory,browserHistory} from 'react-router';
import routes from './routes';
import configureStore from './store/configureStore';
import {Provider} from 'react-redux';
import {loadState,saveState} from './store/localStorage';

// import '../node_modules/bootstrap/dist/js/bootstrap.min.js';
// import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
// import './styles/bootstrap-rtl.min.css';
// import '../node_modules/bootstrap-material-design/dist/css/bootstrap-material-design.min.css';
// import '../node_modules/bootstrap-material-design/dist/css/ripples.min.css';
// import '../node_modules/bootstrap-material-design/dist/js/material.min.js';
// import '../node_modules/bootstrap-material-design/dist/js/ripples.min.js';
import './styles/fonts.css';
import './styles/index.css';



const persistedState = loadState();

const store = configureStore(persistedState);

store.subscribe(() => {
    saveState(store.getState());
});

ReactDOM.render(
    <Provider store={store}>
        <Router history={hashHistory} routes={routes} />
    </Provider>,
  document.getElementById('root')
);
