import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './components/App';
import AboutPage from './components/popups/AboutPage';
import ContactPage from './components/popups/ContactPage';
import RulesPage from './components/popups/RulesPage';
import LoginPage from './components/popups/LoginPage';
import ProductsDetailPage from './components/popups/ProductDetailPage';
import SignUpPage from './components/popups/SignUpPage';
import OrdersInMobile from './components/popups/OrdersInMobile';
import CheckoutPage from './components/main/CheckoutPage';
import MobileVerificationPage from './components/main/MobileVerificationPage';
import CategoryPage from './components/main/categories/CategoryPage';
import ProfilePage from './components/User/ProfilePage';
import OrdersHistoryPage from './components/User/OrdersHistoryPage';
import Order from './components/User/Order';
import SignOut from './components/User/Signout';
import OrderStatus from './components/common/OrderStatus';
import auth from './auth';

function requireAuth(nextState, replace) {
    if (!auth.loggedIn()){
        replace({
            pathname: '/login',
            state: { nextPathname: nextState.location.pathname }
        })
    }
}

function beforeLoginComponent(nextState, replace) {
    if (auth.loggedIn()){
        replace({
            pathname: '/',
            state: { nextPathname: nextState.location.pathname }
        })
    }
}

const scrollToTop = () => {
    window.scrollTo(0,0);
};

export default (
    <Route path="/" component={App} onChange={scrollToTop}>
            <IndexRoute components={{content: CategoryPage}}/>
            <Route path="category/:cid/:cname" components={{content: CategoryPage}}/>
            <Route path="verify" components={{content: MobileVerificationPage}}/>
            <Route path="checkout" components={{content: CheckoutPage}}/>
            <Route path="profile" components={{content: ProfilePage}}/>
            <Route path="history" components={{content: OrdersHistoryPage}}/>
            <Route path="order/:oid" components={{content: Order}}/>
            <Route path="product/:pid/:pname" components={{popup: ProductsDetailPage}}/>
            <Route path="product/new" components={{popup: ProductsDetailPage}}/>
            <Route path='contact' components={{popup: ContactPage}}/>
            <Route path='about' components={{popup: AboutPage}}/>
            <Route path='rules' components={{popup: RulesPage}}/>
            <Route path='login' components={{popup: LoginPage}} onEnter={beforeLoginComponent}/>
            <Route path='logout' components={{content: SignOut}}/>
            <Route path='signup' components={{popup: SignUpPage}} onEnter={beforeLoginComponent}/>
            <Route path="orders" components={{popup: OrdersInMobile}}/>
            <Route path="order-status/:orderId" components={{content:OrderStatus}}/>
    </Route>
);
