import axios from 'axios';
module.exports = {
    login(email, pass,recaptcha, cb) {
        cb = arguments[arguments.length - 1];
        if (localStorage.user) {
            if (cb) cb(true);
            this.onChange(true);
            return
        }
        pretendRequest(email, pass,recaptcha, (res,token) => {
            if (res.authenticated) {
                localStorage.user = JSON.stringify(res);
                localStorage.userToken = token;
                if (cb) cb(true);
                this.onChange(true)
            } else {
                if (cb) cb(false);
                this.onChange(false)
            }
        })
    },

    getToken() {
        let userToken = localStorage.userToken;
        return userToken;
    },
    getUser(){
        let user = localStorage.user;
        user = JSON.parse(user);
        return user;
    },
    updateUser(data){
        localStorage.user = JSON.stringify(data);
    },
    updateToken(data){
        localStorage.token = data;
    },
    logout(cb) {
        delete localStorage.userToken;
        delete localStorage.user;
        if (cb) cb();
        this.onChange(false)
    },

    loggedIn() {
        return !!localStorage.user
    },

    onChange() {}
};

function pretendRequest(username, pass,recaptcha, cb) {
    setTimeout(() => {

        axios.post('https://freevery.com/api/web/user/login', {
            headers: {
                'Content-Type': 'application/json'
            },
            username: username,
            password: pass,
            recaptcha: recaptcha
        })
            .then(function (response) {
                if(response.data.code=='1')
                {
                    let res = response.data.data;
                    res['authenticated'] = true;

                    cb(res,response.data.token);

                }
                else {
                    cb({ authenticated: false })
                }
            })
            .catch(function (error) {
                cb({ authenticated: false })
            });
    }, 0)
}